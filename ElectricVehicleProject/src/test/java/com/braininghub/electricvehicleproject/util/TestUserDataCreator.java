/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danielbodi
 */
public class TestUserDataCreator {

    public static UserData createTestUserData() {
        UserData user = new UserData();

        user.setGroup(createTestGroupData());
        user.setEmail("test@test.hu");
        user.setUsername("user");
        user.setPassword("user");

        return user;
    }

    public static GroupData createTestGroupData() {
        return GroupData.builder()
                .groupname("user")
                .username("user")
                .build();
    }

    public static List<UserData> createTestUserDataList() {
        List<UserData> userDataList = new ArrayList<>();
        userDataList.add(createTestUserData());
        userDataList.add(createTestUserData());
        return userDataList;
    }

}
