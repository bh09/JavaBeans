/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danielbodi
 */
public class TestUserCreator {
    
    public static User createTestUser() {
        User user = new User();
        
        user.setGroup(createTestGroup());
        user.setEmail("test@test.hu");
        user.setUsername("user");
        user.setPassword("user");
        
        return user;
    }
    
    public static Group createTestGroup() {
        return Group.builder()
                .groupname("user")
                .username("user")
                .build();
    }
    
    public static List<User> createTestUserList() {
        List<User> userList = new ArrayList<>();
        userList.add(createTestUser());
        userList.add(createTestUser());
        return userList;
    }
    
}
