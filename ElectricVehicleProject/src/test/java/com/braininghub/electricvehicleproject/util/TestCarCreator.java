/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danielbodi
 */
public class TestCarCreator {

    public static Car createTestCar() {
        Car car = new Car();
        
        car.setBattery(createTestBattery());
        car.setConsumption(createTestConsumption());
        car.setDimensions(createTestDimensions());
        car.setImages(createTestImageList());
        car.setPerformance(createTestPerformance());
        car.setManufacturer("BMW");
        car.setModel("i3");
        car.setPrice(15100000);
        
        return car;
    }

    public static Battery createTestBattery() {
        return Battery.builder()
                .capacity(42.2)
                .chargePortType("CCSIType2")
                .chargeSpeed(257)
                .fastchargePortType("CCS")
                .fastchargeSpeed(290)
                .build();
    }

    public static Consumption createTestConsumption() {
        return Consumption.builder()
                .consumptionEPA(16.1)
                .consumptionNEDC(10.6)
                .consumptionWLTP(12.2)
                .fuelEquivalentEPA(1.8)
                .fuelEquivalentNEDC(1.2)
                .fuelEquivalentWLTP(1.4)
                .rangeEPA(246)
                .rangeNEDC(359)
                .rangeWLTP(310)
                .build();
    }

    public static Dimensions createTestDimensions() {
        return Dimensions.builder()
                .height(1568)
                .length(3998)
                .weight(1345)
                .width(1775)
                .build();
    }

    public static Performance createTestPerformance() {
        return Performance.builder()
                .acceleration(7.3)
                .drivetrain("RWD")
                .powerHp(168)
                .powerKw(125)
                .topSpeed(150)
                .torque(250)
                .build();
    }

    public static Image createTestImage() {
        return Image.builder()
                .description("test")
                .filename("test")
                .build();
    }
    
    public static List<Image> createTestImageList() {
        List<Image> imageList = new ArrayList<>();
        imageList.add(createTestImage());
        imageList.add(createTestImage());
        return imageList;
    }

    public static List<Car> createTestCarList() {
        List<Car> carList = new ArrayList<>();
        carList.add(createTestCar());
        carList.add(createTestCar());
        return carList;
    }

    public static Car createTestCarWithBattery() {
        Car car = new Car();
        Battery battery = new Battery();
        
        battery.setCar(car);
        car.setBattery(battery);
        
        return car;
    }
        
    public static Car createTestCarWithConsumption() {
        Car car = new Car();
        Consumption consumption = new Consumption();
        
        consumption.setCar(car);
        car.setConsumption(consumption);
        
        return car;
    }
    
    public static Car createTestCarWithDimensions() {
        Car car = new Car();
        Dimensions dimensions = new Dimensions();
        
        dimensions.setCar(car);
        car.setDimensions(dimensions);
        
        return car;
    }
    
    public static Car createTestCarWithPerformance() {
        Car car = new Car();
        Performance performance = new Performance();
        
        performance.setCar(car);
        car.setPerformance(performance);
        
        return car;
    }
    
    public static Car createTestCarWithImages() {
        Car car = new Car();
        Image image = new Image();
        List<Image> images = new ArrayList<>();
        images.add(image);
        
        images.forEach(p -> p.setCar(car));
        car.setImages(images);
        
        return car;
    }
    
}
