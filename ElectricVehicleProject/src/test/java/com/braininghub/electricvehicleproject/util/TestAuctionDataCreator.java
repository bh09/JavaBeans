/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.entity.auction.AuctionImage;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivany
 */
public class TestAuctionDataCreator {
    
        private static List<String> l = new ArrayList<>();


    public static AuctionData createTestAuctionData() {
        AuctionData auction = new AuctionData();
        auction.setImages(l);
        auction.setActive(true);
        auction.setCarId(1L);
        auction.setCarTitle("Nissal leaf");
        auction.setCity("Esztergom");
        auction.setCondition("Used");
        auction.setCreationDate(Date.valueOf("2019-07-09"));
        auction.setDescription("description");
        auction.setId(1L);
        auction.setMileage(100);
        auction.setPrice(4980000);
        auction.setProdYear(2010);
        auction.setUser(TestUserDataCreator.createTestUserData());
        return auction;

    }
}
