/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.dto.car.EvParam;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author daniel
 */
public class TestCalculatorDataCreator {
    
    public static List<Integer> createTestPetrolDataList() {
        List<Integer> petrolData = new ArrayList<>();
        
        petrolData.addAll(Arrays.asList(
                8500000, 9068800, 9751360, 10570432, 11553318, 12732782,
                14148138, 15846566, 17884679, 20330415, 23265298));
        
        return petrolData;
    }
    
    public static List<Integer> createTestElectricDataList() {
        List<Integer> electricData = new ArrayList<>();
        
        electricData.addAll(Arrays.asList(
                15100000, 15184525, 15269050, 15353575, 15438100, 15522625,
                15607150, 15691675, 15776200, 15860725, 15945250));
        
        return electricData;
    }
    
    public static EvParam createTestEvParam() {
        return EvParam.builder()
                .carId("1")
                .distance("15000")
                .electricityCost("35")
                .petrolConsumption("8")
                .petrolCost("395")
                .petrolCostIncreases("0.20")
                .petrolVehicleCost("8500000")
                .build();
    }
    
}
