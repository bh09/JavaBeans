/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.util;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.EvParam;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel
 */
public class TestCarDataCreator {
 
    public static CarData createTestCarData() {
        return CarData.builder()
                .battery(createTestBatteryData())
                .consumption(createTestConsumptionData())
                .dimensions(createTestDimensionsData())
                .performance(createTestPerformanceData())
                .images(createTestImageDataList())
                .manufacturer("BMW")
                .model("i3")
                .price(15100000)
                .build();
    }
    
    public static BatteryData createTestBatteryData() {
        return BatteryData.builder()
                .capacity(42.2)
                .chargePortType("CCSIType2")
                .chargeSpeed(257)
                .fastchargePortType("CCS")
                .fastchargeSpeed(290)
                .build();
    }
    
    public static ConsumptionData createTestConsumptionData() {
        return ConsumptionData.builder()
                .consumptionEPA(16.1)
                .consumptionNEDC(10.6)
                .consumptionWLTP(12.2)
                .fuelEquivalentEPA(1.8)
                .fuelEquivalentNEDC(1.2)
                .fuelEquivalentWLTP(1.4)
                .rangeEPA(246)
                .rangeNEDC(359)
                .rangeWLTP(310)
                .build();
    }
   
    public static DimensionsData createTestDimensionsData() {
        return DimensionsData.builder()
                .height(1568)
                .length(3998)
                .weight(1345)
                .width(1775)
                .build();
    }
     
    public static PerformanceData createTestPerformanceData() {
        return PerformanceData.builder()
                .acceleration(7.3)
                .drivetrain("RWD")
                .powerHp(168)
                .powerKw(125)
                .topSpeed(150)
                .torque(250)
                .build();
    }
    
    public static ImageData createTestImageData() {
        return ImageData.builder()
                .description("test")
                .filename("test")
                .build();
    }
    
    public static List<ImageData> createTestImageDataList() {
        List<ImageData> imageDataList = new ArrayList<>();
        imageDataList.add(createTestImageData());
        imageDataList.add(createTestImageData());
        return imageDataList;
    }
    
    public static List<CarData> createTestCarDataList() {
        List<CarData> carDataList = new ArrayList<>();
        carDataList.add(createTestCarData());
        carDataList.add(createTestCarData());
        return carDataList;
    }
    
}
