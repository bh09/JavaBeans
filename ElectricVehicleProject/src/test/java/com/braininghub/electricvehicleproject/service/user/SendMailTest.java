package com.braininghub.electricvehicleproject.service.user;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Roland Schablik
 */
public class SendMailTest {
    
    private SendMail underTest;
    
//    @Before
//    public void setupSendMail() {
//    underTest = new SendMail();
//    }
    
    @Test
    public void testIsValid() {

    //When
    boolean result = underTest.isValid("s!@gmail.com");
    
    //Then
    Assert.assertFalse(result);
    
    }
    
    @Test
    public void testMessage() {
    //Given
    String expectedResult = "Dear test,\nthanks for your registration on "
                + "JavaBeans' Electric Cars community page.";
    
    //When
    String result = underTest.message("test");
    
    //Then
    Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testMessageAuction() {
    //Given
    String expectedResult = "Dear usernameTo,\nOur member, -usernameFrom- is interested in your auction. Contact: emailFrom\nBest regards\nJavaBeans Team";
    
    //When
    String result = underTest.messageAuction("emailFrom", "usernameFrom", "usernameTo");
    
    //Then
    Assert.assertEquals(expectedResult, result);
    }
}
