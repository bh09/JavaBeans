/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import com.braininghub.electricvehicleproject.util.TestCarCreator;
import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author danielbodi
 */
public class CarDataMapperTest {
    
    private CarDataMapper carDataMapper = new CarDataMapper();

    @Test
    public void testMapCarDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Car param = TestCarCreator.createTestCar();
        CarData expectedResult = TestCarDataCreator.createTestCarData();

        //When
        CarData result = CarDataMapper.mapCarDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testMapBatteryDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Battery param = TestCarCreator.createTestBattery();
        BatteryData expectedResult = TestCarDataCreator.createTestBatteryData();

        //When
        BatteryData result = CarDataMapper.mapBatteryDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testMapConsumptionDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Consumption param = TestCarCreator.createTestConsumption();
        ConsumptionData expectedResult = TestCarDataCreator.createTestConsumptionData();

        //When
        ConsumptionData result = CarDataMapper.mapConsumptionDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testMapDimensionsDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Dimensions param = TestCarCreator.createTestDimensions();
        DimensionsData expectedResult = TestCarDataCreator.createTestDimensionsData();

        //When
        DimensionsData result = CarDataMapper.mapDimensionsDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testMapPerformanceDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Performance param = TestCarCreator.createTestPerformance();
        PerformanceData expectedResult = TestCarDataCreator.createTestPerformanceData();

        //When
        PerformanceData result = CarDataMapper.mapPerformanceDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testMapCarDataListFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Car> param = TestCarCreator.createTestCarList();
        List<CarData> expectedResult = TestCarDataCreator.createTestCarDataList();
        
        //When
        List<CarData> result = CarDataMapper.mapCarDataListFromEntity(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testMapImageDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Image param = TestCarCreator.createTestImage();
        ImageData expectedResult = TestCarDataCreator.createTestImageData();

        //When
        ImageData result = CarDataMapper.mapImageDataFromEntity(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImageDataListFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Image> param = TestCarCreator.createTestImageList();
        List<ImageData> expectedResult = TestCarDataCreator.createTestImageDataList();
        
        //When
        List<ImageData> result = CarDataMapper.mapImageDataListFromEntity(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
