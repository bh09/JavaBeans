/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dao.car.CarDao;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.util.TestCarCreator;
import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author danielbodi
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(CarDataMapper.class)
public class CarServiceTest {

    @Mock
    private CarDao carDao;

    @InjectMocks
    private CarService underTest;

    @Before
    public void setupCarService() {
        underTest = new CarService();
        underTest.setCarDao(carDao);
    }

    @Test
    public void testGetModelListForDropdown() {
        //Given
        Mockito.when(carDao.getModelListForDropDown()).thenReturn(new ArrayList<>());

        //When
        List<String> result = underTest.getModelListForDropdown();

        //Then
        Assert.assertEquals(new ArrayList<>(), result);
    }
    
    @Test
    public void testGetManufacturerListForDropdown() {
        //Given
        Mockito.when(carDao.getManufacturerListForDropDown()).thenReturn(new ArrayList<>());

        //When
        List<String> result = underTest.getManufacturerListForDropdown();

        //Then
        Assert.assertEquals(new ArrayList<>(), result);
    }

    @Test
    public void testGetCarDataById() {
        //Given
        Mockito.when(carDao.getCarById(Mockito.anyLong())).thenReturn(TestCarCreator.createTestCar());
        CarData expectedCardata = TestCarDataCreator.createTestCarData();

        //When
        CarData result = underTest.getCarDataById(1L);

        //Then
        Assert.assertEquals(expectedCardata, result);
    }
    
    @Test
    public void testGetCarDataByIdHandlesException() throws IllegalAccessException, InvocationTargetException {
        //Given
        Car car = TestCarCreator.createTestCar();
        CarData expectedCardata = new CarData();
        
        Mockito.when(carDao.getCarById(Mockito.anyLong())).thenReturn(car);
        PowerMockito.mockStatic(CarDataMapper.class);
        PowerMockito.when(CarDataMapper.mapCarDataFromEntity(car)).thenThrow(new IllegalAccessException());

        //When
        CarData result = underTest.getCarDataById(1L);

        //Then
        Assert.assertEquals(expectedCardata, result);
    }

    @Test
    public void testGetAllCarData() {
        //Given
        Mockito.when(carDao.getAllCars()).thenReturn(TestCarCreator.createTestCarList());
        List<CarData> expectedResult = TestCarDataCreator.createTestCarDataList();

        //When
        List<CarData> result = underTest.getAllCarData();

        //Then
        Assert.assertEquals(expectedResult, result);
    }
 
    @Test
    public void testGetAllCarDataHandlesException() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Car> carList = TestCarCreator.createTestCarList();
        List<CarData> expectedResult = new ArrayList<>();
        
        Mockito.when(carDao.getAllCars()).thenReturn(carList);
        PowerMockito.mockStatic(CarDataMapper.class);
        PowerMockito.when(CarDataMapper.mapCarDataListFromEntity(carList)).thenThrow(new IllegalAccessException());

        //When
        List<CarData> result = underTest.getAllCarData();

        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetFilteredCarData() {
        //Given
        Mockito.when(carDao.getFilteredCars(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(TestCarCreator.createTestCarList());
        List<CarData> expectedResult = TestCarDataCreator.createTestCarDataList();
        
        //When
        List<CarData> result = underTest.getFilteredCarData("BMW", "i3");
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetFilteredCarDataHandlesException() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Car> carList = TestCarCreator.createTestCarList();
        List<CarData> expectedResult = new ArrayList<>();
        
        Mockito.when(carDao.getFilteredCars(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(carList);
        PowerMockito.mockStatic(CarDataMapper.class);
        PowerMockito.when(CarDataMapper.mapCarDataListFromEntity(carList)).thenThrow(new IllegalAccessException());
        
        //When
        List<CarData> result = underTest.getFilteredCarData("BMW", "i3");
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetCarDataBySearchParam() {
        //Given
        Mockito.when(carDao.getCarsBySearchParam(Mockito.anyString()))
                .thenReturn(TestCarCreator.createTestCarList());
        List<CarData> expectedResult = TestCarDataCreator.createTestCarDataList();
        
        //When
        List<CarData> result = underTest.getCarDataBySearchParam("test");
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testGetCarDataBySearchParamHandlesException() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Car> carList = TestCarCreator.createTestCarList();
        List<CarData> expectedResult = new ArrayList<>();
        
        Mockito.when(carDao.getCarsBySearchParam(Mockito.anyString()))
                .thenReturn(carList);
        PowerMockito.mockStatic(CarDataMapper.class);
        PowerMockito.when(CarDataMapper.mapCarDataListFromEntity(carList)).thenThrow(new IllegalAccessException());
        
        //When
        List<CarData> result = underTest.getCarDataBySearchParam("test");
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
