/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.entity.auction.Auction;
import com.braininghub.electricvehicleproject.util.TestAuctionCreator;
import com.braininghub.electricvehicleproject.util.TestAuctionDataCreator;
import java.lang.reflect.InvocationTargetException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ivany
 */
public class AuctionEntityMapperTest {

    @Test
    public void testAuctionFromData() throws IllegalAccessException, InvocationTargetException {

        //Given
        AuctionData param = TestAuctionDataCreator.createTestAuctionData();
        Auction expectedResult = TestAuctionCreator.createTestAuction();

        //When
        Auction result = AuctionEntityMapper.mapAuctionFromData(param);

        //Then
        Assert.assertEquals(expectedResult, result);
    }

}
