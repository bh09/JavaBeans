/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.TestUserCreator;
import com.braininghub.electricvehicleproject.util.TestUserDataCreator;
import java.lang.reflect.InvocationTargetException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author danielbodi
 */
public class UserEntityMapperTest {
    
    private UserEntityMapper userEntityMapper = new UserEntityMapper();
    
    @Test
    public void testMapUserFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        UserData param = TestUserDataCreator.createTestUserData();
        User expectedResult = TestUserCreator.createTestUser();
        
        //When
        User result = UserEntityMapper.mapUserFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testMapGroupFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        GroupData param = TestUserDataCreator.createTestGroupData();
        Group expectedResult = TestUserCreator.createTestGroup();
        
        //When
        Group result = UserEntityMapper.mapGroupFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
