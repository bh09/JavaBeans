/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import com.braininghub.electricvehicleproject.util.TestCarCreator;
import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author daniel
 */
public class CarEntityMapperTest {
    
    private CarEntityMapper carEntityMapper = new CarEntityMapper();
        
    @Test
    public void testMapCarFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        CarData param = TestCarDataCreator.createTestCarData();
        Car expectedResult = TestCarCreator.createTestCar();
        
        //When
        Car result = CarEntityMapper.mapCarFromData(param);
        
        //Then
        Assert.assertTrue(expectedResult.equals(result));
    }
    
    @Test
    public void testMapBatteryFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        BatteryData param = TestCarDataCreator.createTestBatteryData();
        Battery expectedResult = TestCarCreator.createTestBattery();
        
        //When
        Battery result = CarEntityMapper.mapBatteryFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
        
    @Test
    public void testMapConsumptionFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        ConsumptionData param = TestCarDataCreator.createTestConsumptionData();
        Consumption expectedResult = TestCarCreator.createTestConsumption();
        
        //When
        Consumption result = CarEntityMapper.mapConsumptionFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
            
    @Test
    public void testMapDimensionsFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        DimensionsData param = TestCarDataCreator.createTestDimensionsData();
        Dimensions expectedResult = TestCarCreator.createTestDimensions();
        
        //When
        Dimensions result = CarEntityMapper.mapDimensionsFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
             
    @Test
    public void testMapPerformanceFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        PerformanceData param = TestCarDataCreator.createTestPerformanceData();
        Performance expectedResult = TestCarCreator.createTestPerformance();
        
        //When
        Performance result = CarEntityMapper.mapPerformanceFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImageListFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<ImageData> param = TestCarDataCreator.createTestImageDataList();
        List<Image> expectedResult = TestCarCreator.createTestImageList();
        
        //When
        List<Image> result = CarEntityMapper.mapImageListFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
             
    @Test
    public void testMapImageFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        ImageData param = TestCarDataCreator.createTestImageData();
        Image expectedResult = TestCarCreator.createTestImage();
        
        //When
        Image result = CarEntityMapper.mapImageFromData(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testUpdateCarFromData() throws IllegalAccessException, InvocationTargetException {
        //Given
        Car paramCar = TestCarCreator.createTestCar();
        CarData paramCarData = TestCarDataCreator.createTestCarData();
        Car expectedResult = TestCarCreator.createTestCar();
        
        //When
        Car result = CarEntityMapper.updateCarFromData(paramCar, paramCarData);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
