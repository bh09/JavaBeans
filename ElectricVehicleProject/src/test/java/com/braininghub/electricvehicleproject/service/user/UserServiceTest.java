/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.service.car.CarDataMapper;
import com.braininghub.electricvehicleproject.util.TestUserCreator;
import com.braininghub.electricvehicleproject.util.TestUserDataCreator;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(CarDataMapper.class)
public class UserServiceTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserService underTest;

    @Before
    public void setupUserService() {
        underTest = new UserService();
        underTest.setUserDao(userDao);
    }

    @Test
    public void testEmailCheck() {
        //Given
        Mockito.when(userDao.isEmailExists(Mockito.anyString()))
                .thenReturn(TestUserCreator.createTestUserList());
        List<UserData> expectedResult = TestUserDataCreator.createTestUserDataList();

        //When
        List<UserData> result = underTest.emailCheck("test@test.hu");

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testUsernameCheck() {
        //Given
        Mockito.when(userDao.isUserExists(Mockito.anyString()))
                .thenReturn(TestUserCreator.createTestUserList());
        List<UserData> expectedResult = TestUserDataCreator.createTestUserDataList();

        //When
        List<UserData> result = underTest.usernameCheck("user");

        //Then
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testGetUserByName() {
        //Given
        Mockito.when(userDao.getUserByName(Mockito.anyString()))
                .thenReturn(TestUserCreator.createTestUser());
        UserData expectedResult = TestUserDataCreator.createTestUserData();

        //When
        UserData result = underTest.getUserByName("user");

        //Then
        Assert.assertEquals(expectedResult, result);
    }

}
