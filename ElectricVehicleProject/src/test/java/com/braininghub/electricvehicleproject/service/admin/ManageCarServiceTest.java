/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.admin;

import com.braininghub.electricvehicleproject.dao.car.CarDao;
import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.service.car.CarDataMapper;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import javax.servlet.http.HttpServletRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author danielbodi
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(CarDataMapper.class)
public class ManageCarServiceTest {
    
    @Mock
    private CarDao carDao;
    
    @Mock
    private HttpServletRequest request;
    
    private ManageCarService underTest;
    
    @Before
    public void setupManageCarService() {
        underTest = new ManageCarService();
        underTest.setCarDao(carDao);
    }
    
    @Test
    public void testRemoveCarById() {
        //Given
        Mockito.when(carDao.getCarById(Mockito.anyLong())).thenReturn(Mockito.any());
        
        //When
        underTest.removeCarById(1L);
        
        //Then
        Mockito.verify(carDao, Mockito.times(1)).getCarById(Mockito.anyLong());
        Mockito.verify(carDao, Mockito.times(1)).removeCar(Mockito.any());
    }
    
    @Test
    public void testInsertCar() {
        //Given
        CarData carData = TestCarDataCreator.createTestCarData();
        
        //When
        underTest.insertCar(carData);
        
        //Then
        Mockito.verify(carDao, Mockito.times(1)).insertCar(Mockito.any());
    }
    
    @Test
    public void testCreateBatteryDataFromRequest() {
        //Given
        Mockito.when(request.getParameter(EvConstants.CAPACITY)).thenReturn("42.2");
        Mockito.when(request.getParameter(EvConstants.CHARGE_PORT_TYPE)).thenReturn("CCSIType2");
        Mockito.when(request.getParameter(EvConstants.CHARGE_SPEED)).thenReturn("257");
        Mockito.when(request.getParameter(EvConstants.FASTCHARGE_PORT_TYPE)).thenReturn("CCS");
        Mockito.when(request.getParameter(EvConstants.FASTCHARGE_SPEED)).thenReturn("290");
        BatteryData expectedResult = TestCarDataCreator.createTestBatteryData();
        
        //When
        BatteryData result = underTest.createBatteryDataFromRequest(request);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
      
    @Test
    public void testCreateConsumptionDataFromRequest() {
        //Given
        Mockito.when(request.getParameter(EvConstants.CONSUMPTION_EPA)).thenReturn("16.1");
        Mockito.when(request.getParameter(EvConstants.CONSUMPTION_NEDC)).thenReturn("10.6");
        Mockito.when(request.getParameter(EvConstants.CONSUMPTION_WLTP)).thenReturn("12.2");
        Mockito.when(request.getParameter(EvConstants.FUEL_EQUIVALENT_EPA)).thenReturn("1.8");
        Mockito.when(request.getParameter(EvConstants.FUEL_EQUIVALENT_NEDC)).thenReturn("1.2");
        Mockito.when(request.getParameter(EvConstants.FUEL_EQUIVALENT_WLTP)).thenReturn("1.4");
        Mockito.when(request.getParameter(EvConstants.RANGE_EPA)).thenReturn("246");
        Mockito.when(request.getParameter(EvConstants.RANGE_NEDC)).thenReturn("359");
        Mockito.when(request.getParameter(EvConstants.RANGE_WLTP)).thenReturn("310");
        ConsumptionData expectedResult = TestCarDataCreator.createTestConsumptionData();
        
        //When
        ConsumptionData result = underTest.createConsumptionDataFromRequest(request);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testCreateDimensionsDataFromRequest() {
        //Given
        Mockito.when(request.getParameter(EvConstants.HEIGHT)).thenReturn("1568");
        Mockito.when(request.getParameter(EvConstants.LENGTH)).thenReturn("3998");
        Mockito.when(request.getParameter(EvConstants.WEIGHT)).thenReturn("1345");
        Mockito.when(request.getParameter(EvConstants.WIDTH)).thenReturn("1775");
        DimensionsData expectedResult = TestCarDataCreator.createTestDimensionsData();
        
        //When
        DimensionsData result = underTest.createDimensionsDataFromRequest(request);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testCreatePerformanceDataFromRequest() {
        //Given
        Mockito.when(request.getParameter(EvConstants.ACCELERATION)).thenReturn("7.3");
        Mockito.when(request.getParameter(EvConstants.DRIVETRAIN)).thenReturn("RWD");
        Mockito.when(request.getParameter(EvConstants.POWER_HP)).thenReturn("168");
        Mockito.when(request.getParameter(EvConstants.POWER_KW)).thenReturn("125");
        Mockito.when(request.getParameter(EvConstants.TOP_SPEED)).thenReturn("150");
        Mockito.when(request.getParameter(EvConstants.TORQUE)).thenReturn("250");
        PerformanceData expectedResult = TestCarDataCreator.createTestPerformanceData();
        
        //When
        PerformanceData result = underTest.createPerformanceDataFromRequest(request);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
