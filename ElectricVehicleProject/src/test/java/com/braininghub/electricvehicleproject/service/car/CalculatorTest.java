/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.EvParam;
import com.braininghub.electricvehicleproject.util.TestCalculatorDataCreator;
import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
 
/**
 *
 * @author danielbodi
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {
    
    @Mock
    private CarService carService;
    
    private Calculator underTest;
    
    @Before
    public void setUpCalculator() {
        underTest = new Calculator();
        underTest.setCarService(carService);
    }
    
    @Test
    public void testPetrolCalculator() {
        //Given
        List<Integer> expectedResult = TestCalculatorDataCreator.createTestPetrolDataList();
        EvParam param =TestCalculatorDataCreator.createTestEvParam();
        
        //When
        List<Integer> result = underTest.petrolCalculator(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testElectricCalculator() {
        //Given
        Mockito.when(carService.getCarDataById(Mockito.anyLong()))
                .thenReturn(TestCarDataCreator.createTestCarData());
        List<Integer> expectedResult = TestCalculatorDataCreator.createTestElectricDataList();
        EvParam param = TestCalculatorDataCreator.createTestEvParam();
        
        //When
        List<Integer> result = underTest.electricCalculator(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testElectricName() {
        //Given
        Mockito.when(carService.getCarDataById(Mockito.anyLong()))
                .thenReturn(TestCarDataCreator.createTestCarData());
        String expectedResult = "BMW i3";
        
        //When
        String result = underTest.electricName("1");
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
}
