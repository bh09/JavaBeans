/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.admin;

import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.util.TestUserCreator;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author daniel
 */
@RunWith(MockitoJUnitRunner.class)
public class ManageUserServiceTest {
    
    @Mock
    private UserDao userDao;
    
    private ManageUserService underTest;
    
    @Before
    public void setupManageUserService() {
        underTest = new ManageUserService();
        underTest.setUserDao(userDao);
    }
    
    @Test
    public void testRemoveUser() {
        //Given
        Mockito.when(userDao.getUserByName(Mockito.anyString()))
                .thenReturn(Mockito.any());
        
        //When
        underTest.removeUser("user");
        
        //Then
        Mockito.verify(userDao, Mockito.times(1)).getUserByName("user");
        Mockito.verify(userDao, Mockito.times(1)).removeUser(Mockito.any());
    }
    
    @Test
    public void testPromoteUserToAdmin() {
        //Given
        Mockito.when(userDao.getUserByName(Mockito.anyString()))
                .thenReturn(TestUserCreator.createTestUser());
        
        //When
        underTest.promoteUserToAdmin("user");
        
        //Then
        Mockito.verify(userDao, Mockito.times(1)).getUserByName("user");
        Mockito.verify(userDao, Mockito.times(1)).updateUser(Mockito.any());
    }
    
    @Test
    public void testDemoteAdminToUser() {
        //Given
        Mockito.when(userDao.getUserByName(Mockito.anyString()))
                .thenReturn(TestUserCreator.createTestUser());
        
        //When
        underTest.demoteAdminToUser("user");
        
        //Then
        Mockito.verify(userDao, Mockito.times(1)).getUserByName("user");
        Mockito.verify(userDao, Mockito.times(1)).updateUser(Mockito.any());
    }
    
    @Test
    public void testGetAllUsername() {
        //Given
        Mockito.when(userDao.getAllUsername()).thenReturn(new ArrayList<>());
        
        //When
        List<String> result = underTest.getAllUsername();
        
        //Then
        Assert.assertEquals(new ArrayList<>(), result);
    }
    
    @Test
    public void testGetAllAdminName() {
        //Given
        Mockito.when(userDao.getAllAdminName()).thenReturn(new ArrayList<>());
        
        //When
        List<String> result = underTest.getAllAdminName();
        
        //Then
        Assert.assertEquals(new ArrayList<>(), result);
    }
    
}
