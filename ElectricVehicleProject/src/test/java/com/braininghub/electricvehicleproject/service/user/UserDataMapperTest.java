/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.TestUserCreator;
import com.braininghub.electricvehicleproject.util.TestUserDataCreator;
import java.lang.reflect.InvocationTargetException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author danielbodi
 */
public class UserDataMapperTest {
    
    private UserDataMapper userDataMapper = new UserDataMapper();
    
    @Test
    public void testMapUserDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        User param = TestUserCreator.createTestUser();
        UserData expectedResult = TestUserDataCreator.createTestUserData();
        
        //When
        UserData result = UserDataMapper.mapUserDataFromEntity(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }
    
    @Test
    public void testMapGroupDataFromEntity() throws IllegalAccessException, InvocationTargetException {
        //Given
        Group param = TestUserCreator.createTestGroup();
        GroupData expectedResult = TestUserDataCreator.createTestGroupData();
        
        //When
        GroupData result = UserDataMapper.mapGroupDataFromEntity(param);
        
        //Then
        Assert.assertEquals(expectedResult, result);
    }

}
