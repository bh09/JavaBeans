/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.car;

import com.braininghub.electricvehicleproject.util.TestCalculatorDataCreator;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author daniel
 */
public class EvParamTest {
    
    @Test
    public void testEqualsAndHashCode() {
        EvParam evParam1 = TestCalculatorDataCreator.createTestEvParam();
        EvParam evParam2 = TestCalculatorDataCreator.createTestEvParam();
        
        Assert.assertEquals(evParam1, evParam2);
        Assert.assertTrue(evParam1.hashCode() == evParam2.hashCode());
    }
    
}
