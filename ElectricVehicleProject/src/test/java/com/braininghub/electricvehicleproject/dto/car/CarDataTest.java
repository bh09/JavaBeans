/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.car;

import com.braininghub.electricvehicleproject.util.TestCarDataCreator;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author danielbodi
 */
public class CarDataTest {
     
    @Test
    public void testEqualsAndHashCode() {
        CarData car1 = TestCarDataCreator.createTestCarData();
        CarData car2 = TestCarDataCreator.createTestCarData();
        
        Assert.assertEquals(car1, car2);
        Assert.assertTrue(car1.hashCode() == car2.hashCode());
    }
    
}
