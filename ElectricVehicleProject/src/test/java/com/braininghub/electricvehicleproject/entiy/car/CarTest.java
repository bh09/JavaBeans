/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entiy.car;

import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import com.braininghub.electricvehicleproject.util.TestCarCreator;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author daniel
 */
public class CarTest {
    
    private Car underTest;
    
    @Before
    public void createCar() {
        underTest = new Car();
    }
  
    @Test
    public void testEqualsAndHashCode() {
        Car car1 = TestCarCreator.createTestCar();
        Car car2 = TestCarCreator.createTestCar();
        
        Assert.assertEquals(car1, car2);
        Assert.assertTrue(car1.hashCode() == car2.hashCode());
    }
    
    @Test
    public void testSetBattery() {
        //Given
        Battery battery = new Battery();
        
        //When
        underTest.setBatteryReference(battery);
        
        //Then
        Assert.assertEquals(underTest.getBattery().getCar(), underTest);
    }
    
    @Test
    public void testSetConsumption() {
        //Given
        Consumption consumption = new Consumption();
        
        //When
        underTest.setConsumptionReference(consumption);
        
        //Then
        Assert.assertEquals(underTest.getConsumption().getCar(), underTest);
    }
    
    @Test
    public void testSetDimensions() {
        //Given
        Dimensions dimensions = new Dimensions();
        
        //When
        underTest.setDimensionsReference(dimensions);
        
        //Then
        Assert.assertEquals(underTest.getDimensions().getCar(), underTest);
    }
    
    @Test
    public void testSetPerformance() {
        //Given
        Performance performance = new Performance();
        
        //When
        underTest.setPerformanceReference(performance);
        
        //Then
        Assert.assertEquals(underTest.getPerformance().getCar(), underTest);
    }
     
    @Test
    public void testSetImages() {
        //Given
        Image image = new Image();
        List<Image> images = new ArrayList<>();
        images.add(image);
        
        //When
        underTest.setImagesReference(images);
        
        //Then
        Assert.assertEquals(underTest.getImages().get(0).getCar(), underTest);
    }
    
}
