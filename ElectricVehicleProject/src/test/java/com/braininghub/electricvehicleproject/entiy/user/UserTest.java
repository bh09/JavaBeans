/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entiy.user;

import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.TestUserCreator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author danielbodi
 */
public class UserTest {
    
    private User underTest;
    
    @Before
    public void createUser() {
        underTest = new User();
    }
    
    @Test
    public void testEqualsAndHashCode() {
        User user1 = TestUserCreator.createTestUser();
        User user2 = TestUserCreator.createTestUser();
        
        Assert.assertEquals(user1, user2);
        Assert.assertTrue(user1.hashCode() == user2.hashCode());
    }
    
    @Test
    public void testSetGroupReference() {
        //Given
        Group group = new Group();
        
        //When
        underTest.setGroupReference(group);
        
        //Then
        Assert.assertEquals(underTest.getGroup().getUser(), underTest);
    }
    
}
