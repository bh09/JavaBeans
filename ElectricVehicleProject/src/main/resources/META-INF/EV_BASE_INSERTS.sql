-- BASIC SETTINGS
USE ev;

DELETE FROM car;
DELETE FROM battery;
DELETE FROM consumption;
DELETE FROM dimensions;
DELETE FROM performance;
DELETE FROM image;
DELETE FROM users;
DELETE FROM user_types;
DELETE FROM auction;
DELETE FROM auction_image;

ALTER TABLE car AUTO_INCREMENT=1;
ALTER TABLE users AUTO_INCREMENT=1;
ALTER TABLE user_types AUTO_INCREMENT=1;
ALTER TABLE auction AUTO_INCREMENT=1;
ALTER TABLE auction_image AUTO_INCREMENT=1;

-- USER INSERTS

INSERT INTO users (email, password, username, regdate, phone) VALUES ('admin@admin.hu', md5('admin'), 'admin', '2019-07-01', '0036304148978');
INSERT INTO users (email, password, username, regdate, phone) VALUES ('admin2@admin.hu', md5('admin'), 'admin2', '2019-03-01','0036304248978');
INSERT INTO users (email, password, username, regdate, phone) VALUES ('admin3admin.hu', md5('admin'), 'admin3', '2019-06-11', '0036304118978');
INSERT INTO users (email, password, username, regdate, phone) VALUES ('user@user.hu', md5('user'), 'user', '2019-01-21', '0036204148978');
INSERT INTO users (email, password, username, regdate, phone) VALUES ('user2@user.hu', md5('user'), 'user2', '2019-03-11', '0036204148978');
INSERT INTO users (email, password, username, regdate, phone) VALUES ('user3@user.hu', md5('user'), 'user3', '2019-01-01', '0036704148978');

INSERT INTO user_types (groupname, username, user_id) VALUES ('admin', 'admin', 1);
INSERT INTO user_types (groupname, username, user_id) VALUES ('admin', 'admin2', 2);
INSERT INTO user_types (groupname, username, user_id) VALUES ('admin', 'admin3', 3);
INSERT INTO user_types (groupname, username, user_id) VALUES ('user', 'user', 4);
INSERT INTO user_types (groupname, username, user_id) VALUES ('user', 'user2', 5);
INSERT INTO user_types (groupname, username, user_id) VALUES ('user', 'user3', 6);

-- CAR INSERTS

INSERT INTO car (manufacturer, model, price) VALUES ('BMW', 'i3', 15100000);
INSERT INTO car (manufacturer, model, price) VALUES ('Fiat', '500', 9430000);
INSERT INTO car (manufacturer, model, price) VALUES ('Nissan', 'Leaf', 11490000);
INSERT INTO car (manufacturer, model, price) VALUES ('Volkswagen', 'e-Up!', 8600000);
INSERT INTO car (manufacturer, model, price) VALUES ('Tesla', 'Model 3', 17200000);
INSERT INTO car (manufacturer, model, price) VALUES ('Tesla', 'Model S', 34500000);
INSERT INTO car (manufacturer, model, price) VALUES ('Jaguar', 'I-Pace', 16974000);
INSERT INTO car (manufacturer, model, price) VALUES ('Smart', 'Fortwo', 3788000);
INSERT INTO car (manufacturer, model, price) VALUES ('Tesla', 'Model X', 21927000);
INSERT INTO car (manufacturer, model, price) VALUES ('Ford', 'Focus', 5290000);
INSERT INTO car (manufacturer, model, price) VALUES ('Kia', 'Niro', 8010000);
INSERT INTO car (manufacturer, model, price) VALUES ('Hyundai', 'Kona', 7719000);

-- IMAGE INSTERTS

INSERT INTO image (description, filename, car_id) VALUES ('BMW i3 - main view', 'bmw_i3_1.png', 1);
INSERT INTO image (description, filename, car_id) VALUES ('BMW i3 - side view', 'bmw_i3_2.png', 1);
INSERT INTO image (description, filename, car_id) VALUES ('BMW i3 - front view', 'bmw_i3_3.png', 1);
INSERT INTO image (description, filename, car_id) VALUES ('BMW i3 - cargo', 'bmw_i3_4.png', 1);
INSERT INTO image (description, filename, car_id) VALUES ('BMW i3 - interior', 'bmw_i3_5.png', 1);

INSERT INTO image (description, filename, car_id) VALUES ('Fiat 500 - main view', 'fiat_500_1.png', 2);
INSERT INTO image (description, filename, car_id) VALUES ('Fiat 500 - side view', 'fiat_500_2.png', 2);
INSERT INTO image (description, filename, car_id) VALUES ('Fiat 500 - front view', 'fiat_500_3.png', 2);
INSERT INTO image (description, filename, car_id) VALUES ('Fiat 500 - cargo', 'fiat_500_4.png', 2);
INSERT INTO image (description, filename, car_id) VALUES ('Fiat 500 - interior', 'fiat_500_5.png', 2);

INSERT INTO image (description, filename, car_id) VALUES ('Nissan Leaf - main view', 'nissan_leaf_1.png', 3);
INSERT INTO image (description, filename, car_id) VALUES ('Nissan Leaf - side view', 'nissan_leaf_2.png', 3);
INSERT INTO image (description, filename, car_id) VALUES ('Nissan Leaf - front view', 'nissan_leaf_3.png', 3);
INSERT INTO image (description, filename, car_id) VALUES ('Nissan Leaf - cargo', 'nissan_leaf_4.png', 3);
INSERT INTO image (description, filename, car_id) VALUES ('Nissan Leaf - interior', 'nissan_leaf_5.png', 3);

INSERT INTO image (description, filename, car_id) VALUES ('Volkswagen e-Up! - main view', 'vw_eup_1.png', 4);
INSERT INTO image (description, filename, car_id) VALUES ('Volkswagen e-Up! - side view', 'vw_eup_2.png', 4);
INSERT INTO image (description, filename, car_id) VALUES ('Volkswagen e-Up! - front view', 'vw_eup_3.png', 4);
INSERT INTO image (description, filename, car_id) VALUES ('Volkswagen e-Up! - back view', 'vw_eup_4.png', 4);
INSERT INTO image (description, filename, car_id) VALUES ('Volkswagen e-Up! - interior', 'vw_eup_5.png', 4);

INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model 3 - main view', 'tesla_model3_1.png', 5);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model 3 - side view', 'tesla_model3_2.png', 5);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model 3 - front view', 'tesla_model3_3.png', 5);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model 3 - back view', 'tesla_model3_4.png', 5);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model 3 - interior', 'tesla_model3_5.png', 5);

INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model S - main view', 'tesla_models_1.png', 6);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model S - side view', 'tesla_models_2.png', 6);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model S - front view', 'tesla_models_3.png', 6);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model S - back view', 'tesla_models_4.png', 6);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model S - interior', 'tesla_models_5.png', 6);

INSERT INTO image (description, filename, car_id) VALUES ('Jaguar I-Pace - main view', 'jaguar_i_pace_1.png', 7);
INSERT INTO image (description, filename, car_id) VALUES ('Jaguar I-Pace - side view', 'jaguar_i_pace_2.png', 7);
INSERT INTO image (description, filename, car_id) VALUES ('Jaguar I-Pace - front view', 'jaguar_i_pace_3.png', 7);
INSERT INTO image (description, filename, car_id) VALUES ('Jaguar I-Pace - back view', 'jaguar_i_pace_4.png', 7);
INSERT INTO image (description, filename, car_id) VALUES ('Jaguar I-Pace - interior', 'jaguar_i_pace_5.png', 7);

INSERT INTO image (description, filename, car_id) VALUES ('Smart Fortwo - main view', 'smart_fortwo_1.png', 8);
INSERT INTO image (description, filename, car_id) VALUES ('Smart Fortwo - side view', 'smart_fortwo_2.png', 8);
INSERT INTO image (description, filename, car_id) VALUES ('Smart Fortwo - front view', 'smart_fortwo_3.png', 8);
INSERT INTO image (description, filename, car_id) VALUES ('Smart Fortwo - back view', 'smart_fortwo_4.png', 8);
INSERT INTO image (description, filename, car_id) VALUES ('Smart Fortwo - interior', 'smart_fortwo_5.png', 8);

INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model X - main view', 'tesla_model_x_1.png', 9);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model X - side view', 'tesla_model_x_2.png', 9);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model X - front view', 'tesla_model_x_3.png', 9);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model X - back view', 'tesla_model_x_4.png', 9);
INSERT INTO image (description, filename, car_id) VALUES ('Tesla Model X - interior', 'tesla_model_x_5.png', 9);

INSERT INTO image (description, filename, car_id) VALUES ('Ford Focus - main view', 'ford_focus_1.png', 10);
INSERT INTO image (description, filename, car_id) VALUES ('Ford Focus - side view', 'ford_focus_2.png', 10);
INSERT INTO image (description, filename, car_id) VALUES ('Ford Focus - front view', 'ford_focus_3.png', 10);
INSERT INTO image (description, filename, car_id) VALUES ('Ford Focus - back view', 'ford_focus_4.png', 10);
INSERT INTO image (description, filename, car_id) VALUES ('Ford Focus - interior', 'ford_focus_5.png', 10);

INSERT INTO image (description, filename, car_id) VALUES ('Kia Niro - main view', 'kia_niro_1.png', 11);
INSERT INTO image (description, filename, car_id) VALUES ('Kia Niro - side view', 'kia_niro_2.png', 11);
INSERT INTO image (description, filename, car_id) VALUES ('Kia Niro - front view', 'kia_niro_3.png', 11);
INSERT INTO image (description, filename, car_id) VALUES ('Kia Niro - back view', 'kia_niro_4.png', 11);
INSERT INTO image (description, filename, car_id) VALUES ('Kia Niro - interior', 'kia_niro_5.png', 11);


INSERT INTO image (description, filename, car_id) VALUES ('Hyundai Kona - main view', 'hyundai_kona_1.png', 12);
INSERT INTO image (description, filename, car_id) VALUES ('Hyundai Kona - side view', 'hyundai_kona_2.png', 12);
INSERT INTO image (description, filename, car_id) VALUES ('Hyundai Kona - front view', 'hyundai_kona_3.png', 12);
INSERT INTO image (description, filename, car_id) VALUES ('Hyundai Kona - back view', 'hyundai_kona_4.png', 12);
INSERT INTO image (description, filename, car_id) VALUES ('Hyundai Kona - interior', 'hyundai_kona_5.png', 12);

-- BATTERY INSERTS

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id) 
VALUES (42.2, 'CCS|Type2', 257, 'CCS', 290, 1);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id) 
VALUES (24, 'N/A', 854, 'N/A', 230, 2);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (62, 'Chademo|Type2', 435, 'CHAdeMO', 240, 3);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id) 
VALUES (18.7, 'CCS|Type2', 854, 'CCS', 230, 4);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (75, 'CCS|Type2', 854, 'CCS', 710, 5);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (100, 'Type2', 854, 'Supercharger', 450, 6);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (90, 'Type 2', 28, 'CCS', 360, 7);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (17.6, 'Type 2', 25, 'N/A', 0, 8);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (75, 'Type 2', 69, 'Supercharger', 370, 9);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (33.5, 'Type 1', 18.2, 'N/A', 0, 10);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (64, 'Type 2', 36, 'CCS', 350, 11);

INSERT INTO battery (capacity, charge_port_type, charge_speed, fastcharge_port_type, fastcharge_speed, car_id)
VALUES (67.1, 'Type 2', 38, 'CCS', 380, 12);

-- CONSUMPTION INSERTS

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (246, 16.1, 1.8, 359, 10.6, 1.2, 310, 12.2, 1.4, 1);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (135, 16, 1.9, 320, 9.9, 1.0, 320, 12.1, 1.2, 2);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (364, 16.5, 1.9, 480, 10.9, 1.2, 385, 14.1, 1.6, 3);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (220, 16.8, 1.9, 160, 10.0, 1.1, 312, 12.0, 1.4, 4);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (499, 15.6, 1.8, 380, 9.2, 1.0, 560, 13.2, 1.5, 5);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (595, 17.5, 2.0, 375, 9.5, 1.0, 610, 16.1, 1.8, 6);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (380, 22.3, 2.5, 543, 15.6, 1.8, 470, 18.0, 2.5, 7);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (105, 15.9, 1.8, 160, 10.4, 1.2, 300, 12, 1.2, 8);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (355, 20.4, 2.3, 400, 9.5, 1.0, 375, 19.3, 2.2, 9);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (185, 15.8, 1.7, 155, 9.1, 0.9, 297, 10.1, 1.1, 10);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (375, 17.1, 1.9, 320, 9.2, 1.0, 455, 14.1, 1.6, 11);

INSERT INTO consumption (range_EPA, consumption_EPA, fuel_Equivalent_EPA, range_NEDC, consumption_NEDC, fuel_Equivalent_NEDC, range_WLTP, consumption_WLTP, fuel_Equivalent_WLTP, car_id) 
VALUES (400, 16, 1.8, 360, 9.5, 1.3, 449, 14.3, 1.6, 12);

-- DIMENSION INSERTS

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (3998, 1775, 1578, 1345, 1);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (3617, 1628, 1527, 1352, 2);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4490, 1788, 1530, 1390, 3);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (3600, 1645, 1504, 1129, 4);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4694, 1933, 1443, 1847, 5);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4979, 1964, 1445, 2215, 6);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4682, 2011, 1565, 2133, 7);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (2695, 1663, 1555, 985, 8);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (5037, 2070, 1684, 2407, 9);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4392, 1824, 1478, 1651, 10);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4375, 1805, 1560, 1737, 11);

INSERT INTO dimensions (LENGTH, width, height, weight, car_id)
VALUES (4180, 1800, 1570, 1685, 12);

-- PERFORMANCE INSERTS

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (7.3, 150, 125, 168, 250, 'RWD', 1);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (9.1, 140, 83, 111, 200, 'FWD', 2);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (6.9, 161, 160, 215, 340, 'FWD', 3);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (12.4, 130, 60, 80, 210, 'FWD', 4);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (3.7, 233, 258, 346, 527, 'AWD', 5);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (3.9, 249, 311, 417, 660, 'AWD', 6);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (4.8, 200, 294, 400, 696, 'AWD', 7);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (11.5, 130, 60, 82, 160, 'Rear', 8);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (4.8, 250, 250, 340, 550, 'AWD', 9);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (9.9, 137, 107, 143, 250, 'FWD', 10);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (7.8, 167, 150, 204, 395, 'Front', 11);

INSERT INTO performance (acceleration, top_speed, power_kw, power_hp, torque, drivetrain, car_id)
VALUES (7.6, 167, 150, 204, 395, 'Front', 12);

-- AUCTION INSERTS

INSERT INTO auction (car_id, car_title, city, car_condition, date_added, description, mileage, price, production_year, user_id, active)
VALUES (1, 'BMW i3', 'Budapest', 'Used', '2019-07-11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum, orci eu.', 105000, 6890000, 2017, 6, true);

INSERT INTO auction (car_id, car_title, city, car_condition, date_added, description, mileage, price, production_year, user_id, active)
VALUES (3, 'Nissan Leaf', 'Esztergom', 'Used', '2019-07-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum, orci eu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum, orci eu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum, orci eu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum', 87000, 4980000, 2018, 5);

INSERT INTO auction (car_id, car_title, city, car_condition, date_added, description, mileage, price, production_year, user_id, active)
VALUES (3, 'Nissan Leaf', 'Esztergom', 'Used', '2019-07-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo erat. 
Vivamus luctus ex nec purus viverra elementum. Fusce elementum, orci eu.', 87000, 4980000, 2018, 5, false);

-- AUCTION IMAGE INSERTS

INSERT INTO auction_image (filename, auction_id) VALUES ('auction_i3_1.jpg', 1);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_i3_2.jpg', 1);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_i3_3.jpg', 1);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_i3_4.jpg', 1);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_i3_5.jpg', 1);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_1.jpg', 2);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_2.jpg', 2);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_3.jpg', 2);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_4.jpg', 2);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_5.jpg', 2);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_1.jpg', 3);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_2.jpg', 3);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_3.jpg', 3);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_4.jpg', 3);
INSERT INTO auction_image (filename, auction_id) VALUES ('auction_leaf_5.jpg', 3);