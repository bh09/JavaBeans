// http://thewebthought.blogspot.com/2012/06/javascript-smooth-scroll-to-top-of-page.html
// <a href="#" onclick="scrollToTop();return false">back to the top of page</a>

var timeOut;
function scrollToTop() {
  if (document.body.scrollTop!=0 || document.documentElement.scrollTop!=0){
    window.scrollBy(0,-50);
    timeOut=setTimeout('scrollToTop()',10);
  }
  else clearTimeout(timeOut);
}

// https://www.w3schools.com/jsref/met_his_back.asp
// <button onclick="goBack()">Go Back</button>

function goBack() {
    window.history.back();
}

// When the user clicks on the button,
// toggle between hiding and showing the dropdown content
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

// http://www.new2html.com/tutorial/using-javascript-change-image-src-attribute/
function changeImage(imgName)
  {
    image = document.getElementById('imgDisp');
    image.src = imgName;
  }