<%-- 
    Document   : successfulreg
    Created on : 2019.07.11., 13:29:20
    Author     : Roland Schablik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Message sent</title>
        <%@include file="./include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Auction message</div>

                    </div>

                    <div class="content-body">

                        <p>
                            Auction message sent successfully. Check your mail box
                            to see response from the sellet. We hope you can make a
                            great trade and enjoy our page.
                        </p>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">Auction message</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

    </body>

</html>
