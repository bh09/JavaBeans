<%-- 
    Document   : singleauction
    Created on : Jul 11, 2019, 11:37:11 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>For sale ${auction.carTitle}</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">${auction.carTitle}</div>
                        <div class="content-subtitle">${auction.user.username}</div>

                    </div>

                    <div class="content-form">

                        <a href="./asset/auction_autos/${auction.images.get(0)}" data-lightbox="car" data-title="${auction.images.get(0)}">
                            <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/auction_autos/${auction.images.get(0)})" alt="${auction.images.get(0)}">
                        </a>

                    </div>

                    <div class="car-page">

                        <div class="thumbnails clearfix">
                            <p>Click to see more images...</p>
                            <c:forEach items="${auction.images}" begin = "1" var="i">
                                <a href="./asset/auction_autos/${i}" data-lightbox="car">
                                    <img src="./asset/auction_autos/${i}" alt="${i}">
                                </a>
                            </c:forEach>
                        </div>

                        <section>
                            <h2>Details</h2>
                            <table class="props">
                                <tr>
                                    <td>Manufacturer and model</td>
                                    <td>${auction.carTitle}</td>
                                </tr>
                                <tr>
                                    <td>Condition</td>
                                    <td>${auction.condition}</td>
                                </tr>
                                <tr>
                                    <td>Mileage</td>
                                    <td>
                                        <script>
                                            document.write(numeral(${auction.mileage}).format('0,0') + " km");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td>First registration</td>
                                    <td>${auction.prodYear}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>${auction.city}</td>
                                </tr>
                                <tr>
                                    <td>Auction owner</td>
                                    <td>${auction.user.username}</td>
                                </tr>
                                <tr>
                                    <td>Auction added</td>
                                    <td>${auction.creationDate}</td>
                                </tr>
                            </table>
                            <a class="button" href="singlecar?id=${auction.carId}">
                                Car specifications
                            </a>
                        </section>

                        <section>
                            <h2>Description</h2>
                            <div class="auction-desc">
                                ${auction.description}
                            </div>
                            <a class="button" href="sendauctionmail?auctid=${auction.id}">
                                Contact seller
                            </a>
                        </section>                       

                    </div>

                    <div class="content-foot">
                        <div class="content-title">${auction.carTitle}</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>  

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                            lightbox.option({
                                                'resizeDuration': 200,
                                                'wrapAround': true,
                                                'alwaysShowNavOnTouchDevices': true
                                            });
        </script>

    </body>

</html>
