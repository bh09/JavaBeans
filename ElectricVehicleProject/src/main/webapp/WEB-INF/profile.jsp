<%-- 
    Document   : profile
    Created on : 2019.07.14., 11:58:30
    Author     : ivany
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>User profile</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Profile</div>
                        <div class="content-subtitle">User data</div>

                    </div>

                    <div class="content-form">

                        <section>
                            <h2>Personal informations</h2>
                            <table class="props">
                                <tr>
                                    <td>Username</td>
                                    <td>${currentUser.username}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>${currentUser.email}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>${currentUser.phone}</td>
                                </tr>
                                <tr>
                                    <td>Registration date</td>
                                    <td>${currentUser.regDate}</td>
                                </tr>
                            </table>
                        </section>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">User data</div>

                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Available auctions</div>
                        <div class="content-subtitle">${activeList.size()} auction(s) found.</div>

                    </div>

                    <div class="content-form">
                        <p>List of your active auctions.</p>
                    </div>

                    <ul class="car-list">

                        <c:forEach items="${activeList}" var="activeauction">
                            <li>
                                <div class="cell-img">
                                    <a href="auction?id=${activeauction.id}">
                                        <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/auction_autos/${activeauction.images.get(0)})" alt="${activeauction.images.get(0)}">
                                    </a>
                                </div>
                                <div class="cell-txt">
                                    <div class="titles">
                                        <div class="title"><a href="auction?id=${activeauction.id}">${activeauction.carTitle}</a></div>
                                        <div class="subtitle">${activeauction.user.username}</div>
                                    </div>
                                    <div class="price">
                                        <script>
                                            document.write(numeral(${activeauction.price}).format('0,0') + " HUF");
                                        </script>
                                    </div>
                                    <table class="single">
                                        <tr>
                                            <td>Year</td>
                                            <td>Condition</td>
                                            <td>Mileage</td>
                                            <td>City</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>${activeauction.prodYear}</td>
                                            <td>${activeauction.condition}</td>
                                            <td>${activeauction.mileage} km</td>
                                            <td>${activeauction.city}</td>
                                            <td>
                                                <form id="#car-filter-form" method ="post">
                                                    <input name="activeauctionid" type="hidden" value="${activeauction.id}"/>
                                                    <input type="submit" value="Archive"/>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                        </c:forEach>

                    </ul>

                    <div class="content-foot">
                        <div class="content-subtitle">${activeList.size()} auction(s) found.</div>
                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Archived auctions</div>
                        <div class="content-subtitle">${archiveList.size()} auction(s) found.</div>

                    </div>

                    <div class="content-form">
                        <p>List of your archived auctions.</p>
                    </div>

                    <ul class="car-list">

                        <c:forEach items="${archiveList}" var="archiveauction">
                            <li>
                                <div class="cell-img">
                                    <a href="auction?id=${archiveauction.id}">
                                        <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/auction_autos/${archiveauction.images.get(0)})" alt="${archiveauction.images.get(0)}">
                                    </a>
                                </div>
                                <div class="cell-txt">
                                    <div class="titles">
                                        <div class="title">${archiveauction.carTitle}</div>
                                        <div class="subtitle">${archiveauction.user.username}</div>
                                    </div>
                                    <div class="price">
                                        <script>
                                            document.write(numeral(${archiveauction.price}).format('0,0') + " HUF");
                                        </script>
                                    </div>
                                    <table class="single">
                                        <tr>
                                            <td>Year</td>
                                            <td>Condition</td>
                                            <td>Mileage</td>
                                            <td>City</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td>${archiveauction.prodYear}</td>
                                            <td>${archiveauction.condition}</td>
                                            <td>${archiveauction.mileage} km</td>
                                            <td>${archiveauction.city}</td>
                                            <td>${archiveauction.creationDate}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                        </c:forEach>

                    </ul>

                    <div class="content-foot">
                        <div class="content-subtitle">${archiveList.size()} auction(s) found.</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                            lightbox.option({
                                                'resizeDuration': 200,
                                                'wrapAround': true,
                                                'alwaysShowNavOnTouchDevices': true
                                            });
        </script>

    </body>

</html>