<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Search result</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Serch result</div>
                        <div class="content-subtitle">${carList.size()} car(s) found.</div>

                    </div>

                    <ul class="car-list">

                        <c:forEach items="${carList}" var="car">
                            <li>
                                <div class="cell-img">
                                    <a href="singlecar?id=${car.id}">
                                        <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/autos/${car.images.get(0).filename})" alt="${car.images.get(0).filename}">
                                    </a>
                                </div>
                                <div class="cell-txt">
                                    <div class="titles">
                                        <div class="title"><a href="singlecar?id=${car.id}">${car.manufacturer} ${car.model}</a></div>
                                        <div class="subtitle">Battery Capacity - ${car.battery.capacity} kWh</div>
                                    </div>
                                    <div class="price">
                                        <script>
                                            document.write(numeral(${car.price}).format('0,0') + " HUF");
                                        </script>
                                    </div>
                                    <table class="single">
                                        <tr>
                                            <td>0 - 100</td>
                                            <td>Top Speed</td>
                                            <td>Range</td>
                                            <td>Efficiency</td>
                                            <td>Fastcharge</td>
                                        </tr>
                                        <tr>
                                            <td>${car.performance.acceleration} sec</td>
                                            <td>${car.performance.topSpeed} km/h</td>
                                            <td>${car.consumption.rangeEPA} km</td>
                                            <td>${car.consumption.consumptionEPA} kWh</td>
                                            <td>${car.battery.fastchargeSpeed} km/h</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                        </c:forEach>

                    </ul>

                    <div class="content-foot">

                        <div class="content-subtitle">${carList.size()} car(s) found.</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                            lightbox.option({
                                                'resizeDuration': 200,
                                                'wrapAround': true,
                                                'alwaysShowNavOnTouchDevices': true
                                            });
        </script>

    </body>

</html>
