<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>${car.manufacturer} ${car.model}</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">
                        <div class="content-title">${car.manufacturer} ${car.model}</div>
                    </div>
                    
                    <div class="content-form">
                        
                        <a href="./asset/autos/${car.images.get(0).filename}" data-lightbox="car" data-title="${car.images.get(0).description}">
                            <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/autos/${car.images.get(0).filename})" alt="${car.images.get(0).filename}">
                        </a>
                        
                    </div>

                    <div class="car-page">
                       
                        <div class="thumbnails clearfix">
                            <p>Click to see more images...</p>
                            <c:forEach items="${car.images}" begin = "1" var="i">
                                <a href="./asset/autos/${i.filename}" data-lightbox="car" data-title="${i.description}">
                                    <img src="./asset/autos/${i.filename}" alt="${i.filename}">
                                </a>
                            </c:forEach>
                        </div>

                        <section>
                            <h2>Performance</h2>
                            <table class="props">
                                <tr>
                                    <td>Acceleration 0 - 100 km/h</td>
                                    <td>${car.performance.acceleration} sec</td>
                                </tr>
                                <tr>
                                    <td>Top Speed</td>
                                    <td>${car.performance.topSpeed} km/h</td>
                                </tr>
                                <tr>
                                    <td>Total Power</td>
                                    <td>${car.performance.powerKw} kW (${car.performance.powerHp} PS)</td>
                                </tr>
                                <tr>
                                    <td>Total Torque</td>
                                    <td>${car.performance.torque} Nm</td>
                                </tr>
                                <tr>
                                    <td>Drivetrain</td>
                                    <td>${car.performance.drivetrain}</td>
                                </tr>
                            </table>
                        </section>

                        <section>
                            <h2>Battery and Charging</h2>
                            <table class="props">
                                <tr>
                                    <td>Battery Capacity</td>
                                    <td>${car.battery.capacity} kWh</td>
                                </tr>
                                <tr>
                                    <td>Charge Port Type</td>
                                    <td>${car.battery.chargePortType}</td>
                                </tr>
                                <tr>
                                    <td>Charge Speed</td>
                                    <td>${car.battery.chargeSpeed} km/h</td>
                                </tr>
                                <tr>
                                    <td>Fastcharge Port Type</td>
                                    <td>${car.battery.fastchargePortType}</td>
                                </tr>
                                <tr>
                                    <td>Fastcharge Speed</td>
                                    <td>${car.battery.fastchargeSpeed} km/h</td>
                                </tr>
                            </table>
                        </section>

                        <section>
                            <h2>Energy Comsumption</h2>
                            <table class="props">
                                <caption>EPA Ratings</caption>
                                <tr>
                                    <td>Range EPA</td>
                                    <td>${car.consumption.rangeEPA} km</td>
                                </tr>
                                <tr>
                                    <td>Consumption EPA</td>
                                    <td>${car.consumption.consumptionEPA} kWh/100km</td>
                                </tr>
                                <tr>
                                    <td>Fuel Equivalent EPA</td>
                                    <td>${car.consumption.fuelEquivalentEPA} l/100km</td>
                                </tr>
                            </table>
                            <table class="props">
                                <caption>NEDC Ratings</caption>
                                <tr>
                                    <td>Range NEDC</td>
                                    <td>${car.consumption.rangeNEDC} km</td>
                                </tr>
                                <tr>
                                    <td>Consumption NEDC</td>
                                    <td>${car.consumption.consumptionNEDC} kWh/100km</td>
                                </tr>
                                <tr>
                                    <td>Fuel Equivalent NEDC</td>
                                    <td>${car.consumption.fuelEquivalentNEDC} l/100km</td>
                                </tr>
                            </table>
                            <table class="props">
                                <caption>WLTP Ratings</caption>
                                <tr>
                                    <td>Range WLTP</td>
                                    <td>${car.consumption.rangeWLTP} km</td>
                                </tr>
                                <tr>
                                    <td>Consumption WLTP</td>
                                    <td>${car.consumption.consumptionWLTP} kWh/100km</td>
                                </tr>
                                <tr>
                                    <td>Fuel Equivalent WLTP</td>
                                    <td>${car.consumption.fuelEquivalentWLTP} l/100km</td>
                                </tr>
                            </table>
                        </section>

                        <section>
                            <h2>Dimensions and Weight</h2>
                            <table class="props">
                                <tr>
                                    <td>Length</td>
                                    <td>${car.dimensions.length} mm</td>
                                </tr>
                                <tr>
                                    <td>Width</td>
                                    <td>${car.dimensions.width} mm</td>
                                </tr>
                                <tr>
                                    <td>Height</td>
                                    <td>${car.dimensions.height} mm</td>
                                </tr>
                                <tr>
                                    <td>Weight Empty</td>
                                    <td>${car.dimensions.weight} kg</td>
                                </tr>
                            </table>
                        </section>

                        <section>
                            <h2>Purchase Price</h2>
                            <table class="props">
                                <tr>
                                    <td>Hungary</td>
                                    <td>
                                        <script>
                                            document.write(numeral(${car.price}).format('0,0') + " HUF");
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </section>

                    </div>

                    <div class="content-foot">
                        <div class="content-title">${car.manufacturer} ${car.model}</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>  

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                            lightbox.option({
                                                'resizeDuration': 200,
                                                'wrapAround': true,
                                                'alwaysShowNavOnTouchDevices': true
                                            });
        </script>

    </body>

</html>
