<%-- 
    Document   : registration
    Created on : Jul 7, 2019, 3:43:51 PM
    Author     : danielbodi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Registration</title>
        <%@include file="./include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Registration</div>
                        <div class="content-subtitle">${subTitle}</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <table class="evspetrol">
                                <tr>
                                    <td>E-mail:</td>
                                    <td><input type="text" name="email"/></td>
                                </tr>
                                <tr>
                                    <td>Username:</td>
                                    <td><input type="text" name="username"/></td>
                                </tr>
                                <tr>
                                    <td>Password:</td>
                                    <td><input type="password" name="password"/></td>
                                </tr>
                                <tr>
                                    <td>Password Confirmation:</td>
                                    <td><input type="password" name="password2"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Registration"/></td>
                                </tr>
                            </table>                           
                        </form>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">Registration</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

    </body>

</html>
