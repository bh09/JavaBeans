<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Electric VS Petrol Car</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">                       
                        <div class="content-title">Electric VS Petrol Car - Return of investment</div>
                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <table class="evspetrol">
                                <tr>
                                    <td>Petrol Vehicle Cost (HUF):</td>
                                    <td><input type="number" value="8500000" name="petrol_vehicle_cost" required></td>
                                </tr>
                                <tr>
                                    <td>Petrol Cost (litre):</td>
                                    <td><input type="number" value="395" name="petrol_cost" required></td>
                                </tr>
                                <tr>
                                    <td>Petrol cost increases per year (E.g. 0.20 = 20% per year):</td>
                                    <td><input type="number" step="0.01" value="0.01" name="petrol_cost_increases" required></td>
                                </tr>
                                <tr>
                                    <td>Expected distance driven every year (km):</td>
                                    <td><input type="number" value="15000" name="distance" required></td>
                                </tr>
                                <tr>
                                    <td>Petrol consumption (litres/100 km):</td>
                                    <td><input type="number" value="8" name="petrol_consumption" required></td>
                                </tr>
                                <tr>
                                    <td>Electricity cost per kWh:</td>
                                    <td><input type="number" value="35" name="electricity_cost" required></td>
                                </tr>
                                <tr>
                                    <td>Electric car selection:</td>
                                    <td>
                                        <select name="car" id="car">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Calculate"></td>
                                </tr>
                            </table>                           
                        </form>

                    </div>

                    <c:if test="${selectBean.succesfulSelect}">

                        <div class="car-page">

                            <div class="canvas-holder">
                                <canvas id="line-chart"></canvas>
                            </div>

                            <script>
                                new Chart(document.getElementById("line-chart"), {
                                    type: 'line',
                                    data: {
                                        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                                        datasets: [{
                                                data: [${petrolCarData.get(0)},
                                ${petrolCarData.get(1)},
                                ${petrolCarData.get(2)},
                                ${petrolCarData.get(3)},
                                ${petrolCarData.get(4)},
                                ${petrolCarData.get(5)},
                                ${petrolCarData.get(6)},
                                ${petrolCarData.get(7)},
                                ${petrolCarData.get(8)},
                                ${petrolCarData.get(9)},
                                ${petrolCarData.get(10)}],
                                                label: "Petrol vehicle",
                                                borderColor: "#0000CD",
                                                fill: false
                                            },
                                            {
                                                data: [${electricCarData.get(0)},
                                ${electricCarData.get(1)},
                                ${electricCarData.get(2)},
                                ${electricCarData.get(3)},
                                ${electricCarData.get(4)},
                                ${electricCarData.get(5)},
                                ${electricCarData.get(6)},
                                ${electricCarData.get(7)},
                                ${electricCarData.get(8)},
                                ${electricCarData.get(9)},
                                ${electricCarData.get(10)}],
                                                label: "Electric vehicle",
                                                borderColor: "#228B22",
                                                fill: false
                                            },
                                        ]
                                    },
                                    options: {
                                        responsive: true,
                                        title: {
                                            display: true,
                                            text: 'Electric VS Petrol Car - Return of Investment'
                                        }
                                    }
                                });
                            </script>

                            <h3>Selected Model: ${carName}</h3>

                        </div>

                    </c:if>

                    <div class="content-foot">                       
                        <div class="content-title">Electric VS Petrol Car - Return of investment</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                lightbox.option({
                                    'resizeDuration': 200,
                                    'wrapAround': true,
                                    'alwaysShowNavOnTouchDevices': true
                                });
        </script>

    </body>
</html>
