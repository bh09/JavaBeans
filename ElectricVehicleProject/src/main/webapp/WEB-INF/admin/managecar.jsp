<%-- 
    Document   : manage
    Created on : Jul 1, 2019, 5:27:23 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Manage cars</title>
        <%@include file="../include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="../include/header.jsp"%>
        </header>

        <nav>
            <%@include file="../include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">
                
                <div class="main-top">
                    <a href="manage" class="button">Manage car</a>
                    <a href="manageuser" class="button">Manage user</a>
                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Delete car</div>
                        <div class="content-subtitle">Select model to delete from the database</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <input type="hidden" name="action" value="delete"/>
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">
                                        <select name="id" class="form-control">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="Delete">
                                    </td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Delete car</div>
                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Update car</div>
                        <div class="content-subtitle">Select model to update in the database</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="get" action="updatecar">
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">
                                        <select name="id" class="form-control">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="Update">
                                    </td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Update car</div>
                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Insert car</div>
                        <div class="content-subtitle">Fill the form to create a new model in the database</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post" action="manage" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="create"/>

                            <section>
                                <h2>Image upload</h2>
                                <table class="evspetrol">                       
                                    <tr>
                                        <td colspan="2">
                                            <input type="file" accept="image/*" name="manage" multiple/>
                                        </td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>General data</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Manufacturer:</td>
                                        <td><input type="text" name="manufacturer" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Model:</td>
                                        <td><input type="text" name="model" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Price:</td>
                                        <td><input type="number" name="price" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Battery and Charging</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Capacity:</td>
                                        <td><input type="number" step="0.1" name="capacity" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Charge port type:</td>
                                        <td><input type="text" name="charge_port_type" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Charge speed:</td>
                                        <td><input type="number" name="charge_speed" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fastcharge port type:</td>
                                        <td><input type="text" name="fastcharge_port_type" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fastcharge speed:</td>
                                        <td><input type="number" name="fastcharge_speed" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Energy Comsumption</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Range EPA:</td>
                                        <td><input type="number" name="range_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption EPA:</td>
                                        <td><input type="number" step="0.1" name="consumption_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent EPA:</td>
                                        <td><input type="number" step="0.1" name="fuel_equivalent_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Range NEDC:</td>
                                        <td><input type="number" name="range_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption NEDC:</td>
                                        <td><input type="number" step="0.1" name="consumption_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent NEDC:</td>
                                        <td><input type="number" step="0.1" name="fuel_equivalent_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Range WLTP:</td>
                                        <td><input type="number" name="range_WLTP" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption WLTP:</td>
                                        <td><input type="number" step="0.1" name="consumption_WLTP" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent WLTP:</td>
                                        <td><input type="number" step="0.1" name="fuel_equivalent_WLTP" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Performance</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Acceleration:</td>
                                        <td><input type="number" step="0.1" name="acceleration" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Top speed:</td>
                                        <td><input type="number" name="top_speed" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Power HP:</td>
                                        <td><input type="number" name="power_hp" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Power kW:</td>
                                        <td><input type="number" name="power_kw" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Torque:</td>
                                        <td><input type="number" name="torque" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Drivetrain:</td>
                                        <td><input type="text" name="drivetrain" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Dimensions and Weight</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Length:</td>
                                        <td><input type="number" name="length" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Width:</td>
                                        <td><input type="number" name="width" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Height:</td>
                                        <td><input type="number" name="height" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Weight:</td>
                                        <td><input type="number" name="weight" required/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input type="submit" value="Upload"/></td>
                                    </tr>
                                </table>
                            </section>

                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Insert car</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="../include/footer.jsp"%>
        </footer>

    </body>

</html>
