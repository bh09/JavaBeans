<%-- 
    Document   : updatecar
    Created on : Jul 10, 2019, 10:02:15 AM
    Author     : danielbodi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Manage cars</title>
        <%@include file="../include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="../include/header.jsp"%>
        </header>

        <nav>
            <%@include file="../include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">
                
                <div class="main-top">
                    <a href="manage" class="button">Manage car</a>
                    <a href="manageuser" class="button">Manage user</a>
                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Update ${car.manufacturer} ${car.model}</div>
                        <div class="content-subtitle">Fill the form to update a model in the database</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">                           
                            <input type="hidden" name="id" value="${car.id}"/>

                            <section>
                                <h2>General data</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Manufacturer:</td>
                                        <td><input type="text" value="${car.manufacturer}" name="manufacturer" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Model:</td>
                                        <td><input type="text" value="${car.model}" name="model" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Price:</td>
                                        <td><input type="number" value="${car.price}" name="price" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Battery and Charging</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Capacity:</td>
                                        <td><input type="number" step="0.1" value="${car.battery.capacity}" name="capacity" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Charge port type:</td>
                                        <td><input type="text" value="${car.battery.chargePortType}" name="charge_port_type" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Charge speed:</td>
                                        <td><input type="number" value="${car.battery.chargeSpeed}" name="charge_speed" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fastcharge port type:</td>
                                        <td><input type="text" value="${car.battery.fastchargePortType}" name="fastcharge_port_type" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fastcharge speed:</td>
                                        <td><input type="number" value="${car.battery.fastchargeSpeed}" name="fastcharge_speed" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Energy Comsumption</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Range EPA:</td>
                                        <td><input type="number" value="${car.consumption.rangeEPA}" name="range_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption EPA:</td>
                                        <td><input type="number" step="0.1" value="${car.consumption.consumptionEPA}" name="consumption_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent EPA:</td>
                                        <td><input type="number" step="0.1" value="${car.consumption.fuelEquivalentEPA}" name="fuel_equivalent_EPA" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Range NEDC:</td>
                                        <td><input type="number" value="${car.consumption.rangeNEDC}" name="range_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption NEDC:</td>
                                        <td><input type="number" value="${car.consumption.consumptionNEDC}" step="0.1" name="consumption_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent NEDC:</td>
                                        <td><input type="number" step="0.1" value="${car.consumption.fuelEquivalentNEDC}" name="fuel_equivalent_NEDC" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Range WLTP:</td>
                                        <td><input type="number" value="${car.consumption.rangeWLTP}" name="range_WLTP" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Consumption WLTP:</td>
                                        <td><input type="number" step="0.1" value="${car.consumption.consumptionWLTP}" name="consumption_WLTP" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Fuel equivalent WLTP:</td>
                                        <td><input type="number" step="0.1" value="${car.consumption.fuelEquivalentWLTP}" name="fuel_equivalent_WLTP" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Performance</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Acceleration:</td>
                                        <td><input type="number" step="0.1" value="${car.performance.acceleration}" name="acceleration" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Top speed:</td>
                                        <td><input type="number" value="${car.performance.topSpeed}" name="top_speed" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Power HP:</td>
                                        <td><input type="number" value="${car.performance.powerHp}" name="power_hp" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Power kW:</td>
                                        <td><input type="number" value="${car.performance.powerKw}" name="power_kw" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Torque:</td>
                                        <td><input type="number" value="${car.performance.torque}" name="torque" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Drivetrain:</td>
                                        <td><input type="text" value="${car.performance.drivetrain}" name="drivetrain" required/></td>
                                    </tr>
                                </table>
                            </section>

                            <section>
                                <h2>Dimensions and Weight</h2>
                                <table class="evspetrol">
                                    <tr>
                                        <td>Length:</td>
                                        <td><input type="number" value="${car.dimensions.length}" name="length" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Width:</td>
                                        <td><input type="number" value="${car.dimensions.width}" name="width" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Height:</td>
                                        <td><input type="number" value="${car.dimensions.height}" name="height" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Weight:</td>
                                        <td><input type="number" value="${car.dimensions.weight}" name="weight" required/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input type="submit" value="Update"/></td>
                                    </tr>
                                </table>
                            </section>

                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Update ${car.manufacturer} ${car.model}</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="../include/footer.jsp"%>
        </footer>

    </body>

</html>
