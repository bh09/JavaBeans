<%-- 
    Document   : manage
    Created on : Jul 1, 2019, 5:27:23 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Manage users</title>
        <%@include file="../include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="../include/header.jsp"%>
        </header>

        <nav>
            <%@include file="../include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="main-top">
                    <a href="manage" class="button">Manage car</a>
                    <a href="manageuser" class="button">Manage user</a>
                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Promote user</div>
                        <div class="content-subtitle">Select user to promote to admin</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <input type="hidden" name="action" value="promote"/>
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">
                                        <select name="username" class="form-control">
                                            <c:forEach items="${usernameList}" var="user">
                                                <option value="${user}">${user}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Promote"></td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Promote user</div>
                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Denote admin</div>
                        <div class="content-subtitle">Select admin to denote to user</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <input type="hidden" name="action" value="demote"/>
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">
                                        <select name="adminname" class="form-control">
                                            <c:forEach items="${adminNameList}" var="admin">
                                                <option value="${admin}">${admin}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Demote"></td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Denote admin</div>
                    </div>

                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Delete user</div>
                        <div class="content-subtitle">Select user to remove from the database</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <input type="hidden" name="action" value="delete"/>
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">
                                        <select name="username" class="form-control">
                                            <c:forEach items="${usernameList}" var="user">
                                                <option value="${user}">${user}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Delete"></td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">
                        <div class="content-subtitle">Delete user</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="../include/footer.jsp"%>
        </footer>

    </body>

</html>
