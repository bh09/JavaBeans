<%-- 
    Document   : createauction
    Created on : Jul 12, 2019, 12:24:19 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Create auction</title>
        <%@include file="./include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Create auction</div>
                        <div class="content-subtitle">Fill the form to create a new auction</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post" action="createauction" enctype="multipart/form-data">
                            <table class="evspetrol">
                                <tr>
                                    <td colspan="2">Choose pictures:</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="file" accept="image/*" name="createauction" multiple/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Select car:</td>
                                    <td>
                                        <select name="car_id" class="form-control">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Title:</td>
                                    <td><input type="text" name="car_title" required/></td>
                                </tr>
                                <tr>
                                    <td>Price:</td>
                                    <td><input type="number" name="price" required/></td>
                                </tr>
                                <tr>
                                    <td>First Registration:</td>
                                    <td><input type="number" min="2010" max="2019" name="production_year" required/></td>
                                </tr>
                                <tr>
                                    <td>Mileage:</td>
                                    <td><input type="number" name="mileage" required/></td>
                                </tr>
                                <tr>
                                    <td>Condition:</td>
                                    <td>
                                        <select name="car_condition" class="form-control">
                                            <option value="new">New</option>
                                            <option value="used">Used</option>
                                        </select>
                                        <!-- <input type="text" name="car_condition" required/> -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>City:</td>
                                    <td><input type="text" name="city" required/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Description:</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><textarea name="description" style="width:100%;height:200px;" spellcheck="false" required></textarea></td>
                                    <!-- <td><input type="text" name="description" required/></td> -->
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Create auction"/></td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">Create auction</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

    </body>
</html>
