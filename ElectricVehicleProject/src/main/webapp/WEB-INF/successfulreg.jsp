<%-- 
    Document   : successfulreg
    Created on : 2019.07.11., 13:29:20
    Author     : Roland Schablik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Registration</title>
        <%@include file="./include/head.jsp"%>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Registration</div>
                        <div class="content-subtitle fw400 cgreen">
                            Successful registration, please check your emails
                        </div>

                    </div>

                    <div class="content-body">

                        <p>
                            Welcome to the world of the future. You can browse the
                            list of electric cars ever made. After logging you can
                            create auctions to sell a car here. Enjoy your stay.
                        </p>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">Registration</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

    </body>

</html>
