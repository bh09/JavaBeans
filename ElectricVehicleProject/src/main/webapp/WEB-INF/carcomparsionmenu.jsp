<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Car comparsion</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">
                        <div class="content-title">Car comparsion</div>
                        <div class="content-subtitle">Select 2 models</div>
                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" method="post">
                            <table class="evspetrol">
                                <tr>
                                    <td>
                                        <select name="car1" class="form-control">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="car2" class="form-control">
                                            <c:forEach items="${carList}" var="car">
                                                <option value="${car.id}">${car.manufacturer} ${car.model}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Apply"></td>
                                </tr>
                            </table>                           
                        </form>

                    </div>

                    <c:if test="${selectBean.succesfulSelect}">

                        <div class="car-page">

                            <table class="compare-img">
                                <tr>
                                    <td><h2>${car1.manufacturer} ${car1.model}</h2><img class="thumb" src="./asset/autos/${car1.images.get(0).filename}" alt="${car1.images.get(0).filename}"></td>
                                    <td><h2>${car2.manufacturer} ${car2.model}</h2><img class="thumb" src="./asset/autos/${car2.images.get(0).filename}" alt="${car1.images.get(0).filename}"></td>
                                </tr>
                            </table>

                            <section>
                                <h3>Purchase Price</h3>
                                <table class="compare">                                    
                                    <tr>
                                        <td>
                                            <script>
                                                document.write(numeral(${car1.price}).format('0,0') + " HUF");
                                            </script>
                                        </td>
                                        <td>Hungary</td>
                                        <td>
                                            <script>
                                                document.write(numeral(${car2.price}).format('0,0') + " HUF");
                                            </script>
                                        </td>
                                    </tr>
                                </table>                                   
                            </section>

                            <section>
                                <h3>Performance Data</h3>
                                <table class="compare">                                    
                                    <tr>
                                        <td>${car1.performance.acceleration} sec</td>
                                        <td>Acceleration</td>
                                        <td>${car2.performance.acceleration} sec</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.performance.topSpeed} km/h</td>
                                        <td>Top Speed</td>
                                        <td>${car2.performance.topSpeed} km/h</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.performance.powerHp} PS</td>
                                        <td>Total Power</td>
                                        <td>${car2.performance.powerHp} PS</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.performance.powerKw} kW</td>
                                        <td>Total Power</td>
                                        <td>${car2.performance.powerKw} kW</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.performance.drivetrain}</td>
                                        <td>Drivetrain</td>
                                        <td>${car2.performance.drivetrain}</td>
                                    </tr>
                                </table>

                                <div class="canvas-holder">
                                    <canvas id="chart2"></canvas>
                                </div>

                                <script>
                                    Chart.defaults.global.animation.duration = 4000;
                                    var ctx = document.getElementById('chart2');
                                    var car1 = "${car1.manufacturer}" + " " + "${car1.model}";
                                    var car2 = "${car2.manufacturer}" + " " + "${car2.model}";

                                    var car1speed = "${car1.performance.topSpeed}";
                                    var car2speed = "${car2.performance.topSpeed}";

                                    var myChart = new Chart(ctx, {
                                        type: 'horizontalBar',
                                        data: {
                                            labels: [car1, car2],
                                            datasets: [{
                                                    label: 'Top Speed',
                                                    data: [car1speed, car2speed],
                                                    backgroundColor: [
                                                        'rgba(174, 212, 230, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)'
                                                    ],
                                                    borderColor: [
                                                        'rgba(174, 212, 230, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)'
                                                    ],
                                                    borderWidth: 1

                                                }]
                                        },
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            scales: {
                                                xAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        },
                                                        gridLines: {
                                                            display: false
                                                        }
                                                    }],
                                                yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        },
                                                        gridLines: {
                                                            display: false
                                                        }
                                                    }]
                                            }
                                        }
                                    });
                                </script>
                            </section>

                            <section>
                                <h3>Battery Data</h3>
                                <table class="compare">                                    
                                    <tr>
                                        <td>${car1.battery.capacity} kWh</td>
                                        <td>Battery Capacity</td>
                                        <td>${car2.battery.capacity} kWh</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.battery.chargePortType}</td>
                                        <td>Charge Port Type</td>
                                        <td>${car2.battery.chargePortType}</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.battery.chargeSpeed} km/h</td>
                                        <td>Charge Speed</td>
                                        <td>${car2.battery.chargeSpeed} km/h</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.battery.fastchargePortType}</td>
                                        <td>Fastcharge Port Type</td>
                                        <td>${car2.battery.fastchargePortType}</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.battery.fastchargeSpeed} km/h</td>
                                        <td>Fastcharge Speed</td>
                                        <td>${car2.battery.fastchargeSpeed} km/h</td>
                                    </tr>
                                </table>                                   
                            </section>

                            <section>
                                <h3>Consumption Data</h3>
                                <table class="compare">                                    
                                    <tr>
                                        <td>${car1.consumption.rangeEPA} km</td>
                                        <td>Range EPA</td>
                                        <td>${car2.consumption.rangeEPA} km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.consumptionEPA} kWh/100km</td>
                                        <td>Consumption EPA</td>
                                        <td>${car2.consumption.consumptionEPA} kWh/100km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.fuelEquivalentEPA} l/100km</td>
                                        <td>Fuel Equivalent EPA</td>
                                        <td>${car2.consumption.fuelEquivalentEPA} l/100km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.rangeNEDC} km</td>
                                        <td>Range NEDC</td>
                                        <td>${car2.consumption.rangeNEDC} km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.consumptionNEDC} kWh/100km</td>
                                        <td>Consumption NEDC</td>
                                        <td>${car2.consumption.consumptionNEDC} kWh/100km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.fuelEquivalentNEDC} l/100km</td>
                                        <td>Fuel Equivalent NEDC</td>
                                        <td>${car2.consumption.fuelEquivalentNEDC} l/100km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.rangeWLTP} km</td>
                                        <td>Range WLTP</td>
                                        <td>${car2.consumption.rangeWLTP} km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.consumptionWLTP} kWh/100km</td>
                                        <td>Consumption WLTP</td>
                                        <td>${car2.consumption.consumptionWLTP} kWh/100km</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.consumption.fuelEquivalentWLTP} l/100km</td>
                                        <td>Fuel Equivalent WLTP</td>
                                        <td>${car2.consumption.fuelEquivalentWLTP} l/100km</td>
                                    </tr>
                                </table>

                                <div class="canvas-holder">
                                    <canvas id="chart1"></canvas>
                                </div>

                                <script>
                                    Chart.defaults.global.animation.duration = 4000;
                                    var ctx = document.getElementById('chart1');
                                    var car1 = "${car1.manufacturer}" + " " + "${car1.model}";
                                    var car2 = "${car2.manufacturer}" + " " + "${car2.model}";

                                    var car1comp = "${car1.consumption.rangeEPA}";
                                    var car2comp = "${car2.consumption.rangeEPA}";

                                    var myChart = new Chart(ctx, {
                                        type: 'horizontalBar',
                                        data: {
                                            labels: [car1, car2],
                                            datasets: [{
                                                    label: 'Range EPA',
                                                    data: [car1comp, car2comp],
                                                    backgroundColor: [
                                                        'rgba(174, 212, 230, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)'
                                                    ],
                                                    borderColor: [
                                                        'rgba(174, 212, 230, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)',
                                                        'rgba(53, 126, 175, 1)',
                                                        'rgba(152, 226, 198, 1)'
                                                    ],
                                                    borderWidth: 1

                                                }]
                                        },
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            scales: {
                                                xAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        },
                                                        gridLines: {
                                                            display: false
                                                        }
                                                    }],
                                                yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        },
                                                        gridLines: {
                                                            display: false
                                                        }
                                                    }]
                                            }
                                        }
                                    });
                                </script>                                
                            </section>

                            <section>
                                <h3>Dimensions Data</h3>
                                <table class="compare">                                   
                                    <tr>
                                        <td>${car1.dimensions.length} mm</td>
                                        <td>Length</td>
                                        <td>${car2.dimensions.length} mm</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.dimensions.width} mm</td>
                                        <td>Width</td>
                                        <td>${car2.dimensions.width} mm</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.dimensions.height} mm</td>
                                        <td>Height</td>
                                        <td>${car2.dimensions.height} mm</td>
                                    </tr>
                                    <tr>
                                        <td>${car1.dimensions.weight} kg</td>
                                        <td>Weight</td>
                                        <td>${car2.dimensions.weight} kg</td>
                                    </tr>
                                </table>
                            </section>

                        </div>

                    </c:if>

                    <div class="content-foot">
                        <div class="content-subtitle">Car comparsion</div>
                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                    lightbox.option({
                                        'resizeDuration': 200,
                                        'wrapAround': true,
                                        'alwaysShowNavOnTouchDevices': true
                                    });
        </script>

    </body>

</html>
