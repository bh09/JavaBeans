<%-- 
    Document   : auctionlist
    Created on : Jul 11, 2019, 9:53:23 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Auction list</title>
        <%@include file="./include/head.jsp"%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    </head>

    <body>

        <header>
            <%@include file="./include/header.jsp"%>
        </header>

        <nav>
            <%@include file="./include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="main-top">
                    <a href="createauction" class="button">Create a new auction</a>
                </div>

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Available auctions</div>
                        <div class="content-subtitle">${auctionList.size()} auction(s) found.</div>

                    </div>

                    <div class="content-form">
                        <p>You must be logged in to create a new auction or contact a seller.</p>
                    </div>

                    <ul class="car-list">

                        <c:forEach items="${auctionList}" var="auction">
                            <li>
                                <div class="cell-img">
                                    <a href="auction?id=${auction.id}">
                                        <img class="thumb img-with-bg" src="./asset/img/thumb.png" style="background-image:url(./asset/auction_autos/${auction.images.get(0)})" alt="${auction.images.get(0)}">
                                    </a>
                                </div>
                                <div class="cell-txt">
                                    <div class="titles">
                                        <div class="title"><a href="auction?id=${auction.id}"><strong>${auction.carTitle}</strong></a></div>
                                        <div class="subtitle">${auction.user.username}</div>
                                    </div>
                                    <div class="price">
                                        <script>
                                            document.write(numeral(${auction.price}).format('0,0') + " HUF");
                                        </script>
                                    </div>
                                    <table class="single">
                                        <tr>
                                            <td>Year</td>
                                            <td>Condition</td>
                                            <td>Mileage</td>
                                            <td>City</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td>${auction.prodYear}</td>
                                            <td>${auction.condition}</td>
                                            <td>
                                                <script>
                                                    document.write(numeral(${auction.mileage}).format('0,0') + " km");
                                                </script>
                                            </td>
                                            <td>${auction.city}</td>
                                            <td>${auction.creationDate}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                        </c:forEach>    

                    </ul>

                    <div class="content-foot">

                        <div class="content-subtitle">${auctionList.size()} auction(s) found.</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="./include/footer.jsp"%>
        </footer>

        <script src="./asset/js/my.js"></script>
        <script src="./asset/lightbox/js/lightbox-plus-jquery.js"></script>
        <script>
                                                    lightbox.option({
                                                        'resizeDuration': 200,
                                                        'wrapAround': true,
                                                        'alwaysShowNavOnTouchDevices': true
                                                    });
        </script>

    </body>

</html>
