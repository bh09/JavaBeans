<%-- 
    Document   : head
    Created on : Jun 26, 2019, 9:45:48 AM
    Author     : daniel
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="wrapper">

    <div class="section clearfix">

        <div class="col-4">
            <div id="logo">
                <a href="carlist">JavaBeans<em>Electric Vehicles</em></a>
            </div>
        </div>

        <div class="col-4">
            <div id="search">
                <form id="search-form" action="search" method="post">
                    <input type="text" name="filter" value="" placeholder="Search...">
                    <input type="submit" value="">             
                </form>
            </div>
        </div>

        <div class="col-4">
            <div id="user-buttons">
                <%--<c:set scope="session" var="request.isUserInRole()" />--%>

                <% request.setAttribute("admin", request.isUserInRole("admin")); %>
                <% request.setAttribute("user", request.isUserInRole("user"));%>

                <c:choose>
                    <c:when test="${requestScope.user}">
                        <a href="profile">Profile</a>  
                        <a href="logout">Logout</a>
                    </c:when>
                    <c:when test="${requestScope.admin}">
                        <a href="manage">Manage</a>
                        <a href="logout">Logout</a>
                    </c:when>
                    <c:otherwise>
                        <a href="login">Login</a>
                        <a href="registration">Registration</a>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>

    </div>

</div>
