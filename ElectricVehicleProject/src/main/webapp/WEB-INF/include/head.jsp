<%-- 
    Document   : head
    Created on : Jun 26, 2019, 9:57:18 AM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta name="author" content="JavaBeans">
<meta name="description" content="Electric car database with comparsion of ev to petrol cars.">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="./asset/css/reset.css">
<link rel="stylesheet" href="./asset/css/main.css">
<link rel="stylesheet" href="./asset/lightbox/css/lightbox.css">
