<%-- 
    Document   : nav
    Created on : Jun 26, 2019, 9:49:23 AM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="wrapper">
    
    <ul class="clearfix">
        <li><a href="carlist">Car List</a></li>
        <li><a href="carcomparsion">Compare Cars</a></li>
        <li><a href="electricvspetrol">Petrol vs EV</a></li>
        <li><a href="auctionlist">Market Place</a></li>
    </ul>
    
</div>
