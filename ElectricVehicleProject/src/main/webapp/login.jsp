<%-- 
    Document   : login
    Created on : 2019.07.09., 10:50:44
    Author     : ivany
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Registration</title>
        <%@include file="/WEB-INF/include/head.jsp"%>

    </head>

    <body>

        <header>
            <%@include file="/WEB-INF/include/header.jsp"%>
        </header>

        <nav>
            <%@include file="/WEB-INF/include/nav.jsp"%>
        </nav>

        <main>

            <div class="wrapper">

                <div class="content">

                    <div class="content-head">

                        <div class="content-title">Login</div>
                        <div class="content-subtitle">Fill the form to login</div>

                    </div>

                    <div class="content-form">

                        <form id="car-filter-form" action="j_security_check" method="post">
                            <table class="evspetrol">
                                <tr>
                                    <td>Username:</td>
                                    <td><input type="text" name="j_username"/></td>
                                </tr>
                                <tr>
                                    <td>Password:</td>
                                    <td><input type="password" name="j_password"/></td>
                                <tr>
                                    <td colspan="2"><input type="submit" value="Login"/></td>
                                </tr>
                            </table>
                        </form>

                    </div>

                    <div class="content-foot">

                        <div class="content-subtitle">Login</div>

                    </div>

                </div>

            </div>

        </main>

        <footer>
            <%@include file="/WEB-INF/include/footer.jsp"%>
        </footer>

    </body>

</html>

