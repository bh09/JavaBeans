package com.braininghub.electricvehicleproject.util;

import javax.servlet.http.Part;

public class EvUtil {

    public static String speedNrToTimeStr(int intTime) {

        StringBuilder speedStr = new StringBuilder("");

        if (intTime == 0) {
            speedStr.append("unknown");
        } else {
            speedStr.append(intTime / EvConstants.HOUR).append("h");
            speedStr.append(intTime - (intTime / EvConstants.HOUR) * EvConstants.HOUR).append("m");
        }

        return speedStr.toString();

    }

    public static String priceToStr(int intPrice) {

        String modStr;

        if (intPrice == 0) {
            return "";
        } else {
            if (intPrice >= EvConstants.THOUSAND) {
                modStr = "000" + String.valueOf(intPrice % EvConstants.THOUSAND);
                modStr = modStr.substring(modStr.length() - 3);
            } else {
                modStr = String.valueOf(intPrice % EvConstants.THOUSAND);
            }
            return priceToStr(intPrice / EvConstants.THOUSAND) + modStr + " ";
        }

    }
    
    public static String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
    
}
