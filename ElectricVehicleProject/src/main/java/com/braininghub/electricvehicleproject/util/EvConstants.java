package com.braininghub.electricvehicleproject.util;

public class EvConstants {

    //  CAR

    public static final String CAR = "car";
    
    public static final String ID = "id";
    public static final String MODEL = "model";
    public static final String PRICE = "price";
    public static final String MANUFACTURER = "manufacturer";

    public static final String CAR_ONE = "car1";
    public static final String CAR_TWO = "car2";

    public static final String CAR_LIST = "carList";
    public static final String MODEL_LIST = "modelList";
    public static final String MANUFACTURER_LIST = "manufacturerList";
    
    //  BATTERY
    
    public static final String BATTERY = "battery";
    
    public static final String CAPACITY = "capacity";
    public static final String CHARGE_PORT_TYPE= "charge_port_type";
    public static final String CHARGE_SPEED = "charge_speed";
    public static final String FASTCHARGE_PORT_TYPE = "fastcharge_port_type";
    public static final String FASTCHARGE_SPEED = "fastcharge_speed";
    
    //  CONSUMPTION
    
    public static final String CONSUMPTION = "consumption";
    
    public static final String RANGE_EPA = "range_EPA";
    public static final String CONSUMPTION_EPA = "consumption_EPA";
    public static final String FUEL_EQUIVALENT_EPA = "fuel_equivalent_EPA";
    
    public static final String RANGE_NEDC = "range_NEDC";
    public static final String CONSUMPTION_NEDC = "consumption_NEDC";
    public static final String FUEL_EQUIVALENT_NEDC = "fuel_equivalent_NEDC";
    
    public static final String RANGE_WLTP = "range_WLTP";
    public static final String CONSUMPTION_WLTP = "consumption_WLTP";
    public static final String FUEL_EQUIVALENT_WLTP = "fuel_equivalent_WLTP";
    
    //  DIMENSIONS
    
    public static final String DIMENSIONS = "dimensions";
    
    public static final String LENGTH = "length";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    
    //  IMAGE
    
    public static final String IMAGE = "image";
    
    public static final String FILENAME = "filename";
    public static final String DESCRIPTION = "description";
    
    public static final String IMAGE_PATH = "./asset/autos/";
    
    //  PERFORMANCE
    
    public static final String PERFORMANCE = "performance";
    
    public static final String ACCELERATION = "acceleration";
    public static final String TOP_SPEED = "top_speed";
    public static final String POWER_HP = "power_hp";
    public static final String POWER_KW = "power_kw";
    public static final String TORQUE = "torque";
    public static final String DRIVETRAIN = "drivetrain";
    
    //  USERS AND GROUPS
    
    public static final String USERS = "users";
    public static final String GROUPS = "groups";
    
    public static final String VALIDATION = "validation";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String PASSWORD2 = "password2";
    public static final String EMAIL = "email";
    public static final String GROUPNAME = "groupname";
    
    public static final String USER = "user";
    public static final String ADMIN = "admin";
    public static final String USER_TYPES = "user_types";
    
    public static final String REGISTRATION_SUBJECT = "Registration complete";
    
    //  AUCTION
    
    public static final String AUCTION_ID = "auctid";
    public static final String AUCTION_SUBJECT = "New auction interest";
    public static final String AUCTION = "auction";
    public static final String AUCTION_IMAGE = "auction_image";
    public static final String AUCTION_LIST = "auctionList";
    
    public static final String MILEAGE = "mileage";
    public static final String PRODUCTION_YEAR = "production_year";
    public static final String DATE_ADDED = "date_added";
    public static final String CAR_CONDITION = "car_condition";
    public static final String CITY = "city";
    public static final String CAR_TITLE = "car_title";
    public static final String CAR_ID = "car_id";
    
    //  PERSISTENCE
    
    public static final String PU_NAME = "com.braininghub_ElectricVehicleProject_war_1.0-SNAPSHOTPU";

    //  MISC
    
    public static final String WILDCARD = "%";
    public static final String SELECT_BEAN = "selectBean";
    public static final int CHART_PRECISION = 1_000_000;
    public static final int HOUR = 60;
    public static final int THOUSAND = 1_000;

}
