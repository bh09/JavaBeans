/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author daniel
 */
public class CarEntityMapper {

    public static Car mapCarFromData(CarData carData)
            throws IllegalAccessException, InvocationTargetException {

        Car car = new Car();

        car.setBatteryReference(mapBatteryFromData(carData.getBattery()));
        car.setConsumptionReference(mapConsumptionFromData(carData.getConsumption()));
        car.setDimensionsReference(mapDimensionsFromData(carData.getDimensions()));
        car.setImagesReference(mapImageListFromData(carData.getImages()));
        car.setPerformanceReference(mapPerformanceFromData(carData.getPerformance()));
        car.setManufacturer(carData.getManufacturer());
        car.setModel(carData.getModel());
        car.setPrice(carData.getPrice());

        return car;
    }

    public static Battery mapBatteryFromData(BatteryData batteryData)
            throws IllegalAccessException, InvocationTargetException {

        Battery battery = new Battery();
        BeanUtils.copyProperties(battery, batteryData);
        return battery;
    }

    public static Consumption mapConsumptionFromData(ConsumptionData consumptionData)
            throws IllegalAccessException, InvocationTargetException {

        Consumption consumption = new Consumption();
        BeanUtils.copyProperties(consumption, consumptionData);
        return consumption;
    }

    public static Dimensions mapDimensionsFromData(DimensionsData dimensionsData)
            throws IllegalAccessException, InvocationTargetException {

        Dimensions dimensions = new Dimensions();
        BeanUtils.copyProperties(dimensions, dimensionsData);
        return dimensions;
    }

    public static Performance mapPerformanceFromData(PerformanceData performanceData)
            throws IllegalAccessException, InvocationTargetException {

        Performance performance = new Performance();
        BeanUtils.copyProperties(performance, performanceData);
        return performance;
    }

    public static List<Image> mapImageListFromData(List<ImageData> imageDataList)
            throws IllegalAccessException, InvocationTargetException {

        List<Image> imageList = new ArrayList<>();

        for (ImageData imageData : imageDataList) {
            imageList.add(mapImageFromData(imageData));
        }

        return imageList;
    }

    public static Image mapImageFromData(ImageData imageData)
            throws IllegalAccessException, InvocationTargetException {

        Image image = new Image();
        BeanUtils.copyProperties(image, imageData);
        return image;
    }

    public static Car updateCarFromData(Car car, CarData carData)
            throws IllegalAccessException, InvocationTargetException {
        
        BeanUtils.copyProperties(car.getBattery(), carData.getBattery());
        BeanUtils.copyProperties(car.getConsumption(), carData.getConsumption());
        BeanUtils.copyProperties(car.getDimensions(), carData.getDimensions());
        BeanUtils.copyProperties(car.getPerformance(), carData.getPerformance());
        car.setModel(carData.getModel());
        car.setManufacturer(carData.getManufacturer());
        car.setPrice(carData.getPrice());
        
        return car;
    }

}
