/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.entity.car.Battery;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.entity.car.Consumption;
import com.braininghub.electricvehicleproject.entity.car.Dimensions;
import com.braininghub.electricvehicleproject.entity.car.Image;
import com.braininghub.electricvehicleproject.entity.car.Performance;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author danielbodi
 */
public class CarDataMapper {

    public static CarData mapCarDataFromEntity(Car car)
            throws IllegalAccessException, InvocationTargetException {

        return CarData.builder().battery(mapBatteryDataFromEntity(car.getBattery()))
                .consumption(mapConsumptionDataFromEntity(car.getConsumption()))
                .dimensions(mapDimensionsDataFromEntity(car.getDimensions()))
                .performance(mapPerformanceDataFromEntity(car.getPerformance()))
                .images(mapImageDataListFromEntity(car.getImages()))
                .manufacturer(car.getManufacturer())
                .model(car.getModel())
                .price(car.getPrice())
                .id(car.getId())
                .build();
    }

    public static BatteryData mapBatteryDataFromEntity(Battery battery)
            throws IllegalAccessException, InvocationTargetException {

        BatteryData batteryData = new BatteryData();
        BeanUtils.copyProperties(batteryData, battery);
        return batteryData;
    }

    public static ConsumptionData mapConsumptionDataFromEntity(Consumption consumption)
            throws IllegalAccessException, InvocationTargetException {

        ConsumptionData consumptionData = new ConsumptionData();
        BeanUtils.copyProperties(consumptionData, consumption);
        return consumptionData;
    }

    public static DimensionsData mapDimensionsDataFromEntity(Dimensions dimensions)
            throws IllegalAccessException, InvocationTargetException {

        DimensionsData dimensionsData = new DimensionsData();
        BeanUtils.copyProperties(dimensionsData, dimensions);
        return dimensionsData;
    }

    public static PerformanceData mapPerformanceDataFromEntity(Performance performance)
            throws IllegalAccessException, InvocationTargetException {

        PerformanceData performanceData = new PerformanceData();
        BeanUtils.copyProperties(performanceData, performance);
        return performanceData;
    }
    
    public static List<ImageData> mapImageDataListFromEntity(List<Image> imageList)
            throws IllegalAccessException, InvocationTargetException {
        
        List<ImageData> imageDataList = new ArrayList<>();
        
        for (Image imageData : imageList) {
            imageDataList.add(mapImageDataFromEntity(imageData));
        }
        return imageDataList;
    }

    public static List<CarData> mapCarDataListFromEntity(List<Car> carList)
            throws IllegalAccessException, InvocationTargetException {

        List<CarData> carDataList = new ArrayList<>();

        for (Car car : carList) {
            carDataList.add(CarDataMapper.mapCarDataFromEntity(car));
        }
        return carDataList;
    }
    
    public static ImageData mapImageDataFromEntity(Image image)
            throws IllegalAccessException, InvocationTargetException {
        
        ImageData imageData = new ImageData();
        BeanUtils.copyProperties(imageData, image);
        return imageData;
    }

}
