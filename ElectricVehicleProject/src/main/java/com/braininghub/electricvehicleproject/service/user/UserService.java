/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.google.common.annotations.VisibleForTesting;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author danielbodi
 */
@Stateless
public class UserService {

    @Inject
    private UserDao userDao;
    
    @VisibleForTesting
    void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void insertUser(UserData userData) {
        try {
            userDao.insertUser(UserEntityMapper.mapUserFromData(userData));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            System.out.println(ex.getMessage());
        }
    }

//    public List<User> emailCheck(String email) {
//        return userDao.isEmailExists(email);
//    }
    
    public List<UserData> emailCheck(String email) {
        try {
            return UserDataMapper.mapUserDataListFromEntity(userDao.isEmailExists(email));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }

//    public List<User> usernameCheck(String username) {
//        return userDao.isUserExists(username);
//    }
    
    public List<UserData> usernameCheck(String username) {
        try {
            return UserDataMapper.mapUserDataListFromEntity(userDao.isUserExists(username));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }

    public UserData getUserByName(String username) {
        try {
            return UserDataMapper.mapUserDataFromEntity(userDao.getUserByName(username));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new UserData();
        }
    }
    
}
