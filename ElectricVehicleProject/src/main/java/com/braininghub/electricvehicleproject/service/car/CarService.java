package com.braininghub.electricvehicleproject.service.car;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.braininghub.electricvehicleproject.dao.car.CarDao;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.google.common.annotations.VisibleForTesting;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author danielbodi
 */
@Stateless
public class CarService {

    @Inject
    private CarDao carDao;

    @VisibleForTesting
    void setCarDao(CarDao carDao) {
        this.carDao = carDao;
    }

    public List<String> getManufacturerListForDropdown() {
        return carDao.getManufacturerListForDropDown();
    }

    public List<String> getModelListForDropdown() {
        return carDao.getModelListForDropDown();
    }

    public CarData getCarDataById(Long id) {
        try {
            return CarDataMapper.mapCarDataFromEntity(carDao.getCarById(id));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new CarData();
        }
    }

    public List<CarData> getAllCarData() {
        try {
            return CarDataMapper.mapCarDataListFromEntity(carDao.getAllCars());
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
    
    public List<CarData> getFilteredCarData(String manufacturer, String model) {
        try {
            return CarDataMapper.mapCarDataListFromEntity(carDao.getFilteredCars(manufacturer, model));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
    
    public List<CarData> getCarDataBySearchParam(String search) {
        try {
            return CarDataMapper.mapCarDataListFromEntity(carDao.getCarsBySearchParam(search));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }

}
