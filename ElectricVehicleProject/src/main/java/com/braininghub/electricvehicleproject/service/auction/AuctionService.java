/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.auction;

import com.braininghub.electricvehicleproject.dao.car.AuctionDao;
import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.auction.Auction;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.service.user.UserDataMapper;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author daniel
 */
@Stateless
public class AuctionService {
    
    @Inject
    private AuctionDao auctionDao;
    
    @Inject
    private UserDao userDao;
    
    public void insertAuction(AuctionData auctionData) {
        try {
            Auction auction = AuctionEntityMapper.mapAuctionFromData(auctionData);
            User user = userDao.getUserByName(auctionData.getUser().getUsername());
            auction.setUser(user);
            auctionDao.insertAuction(auction);
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Error during auction insert.");
        }
    }
    
    public List<AuctionData> getAllAuctions() {
        try {
            return AuctionDataMapper.mapAuctionDatalistFromEnytity(auctionDao.getAllAuctions());
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
    
    public List<AuctionData> getActiveAuctions() {
        try {
            return AuctionDataMapper.mapAuctionDatalistFromEnytity(auctionDao.getActiveAuctions());
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
    
     public List<AuctionData> getArchiveAuctions() {
        try {
            return AuctionDataMapper.mapAuctionDatalistFromEnytity(auctionDao.getArchiveAuctions());
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
     
     public List<AuctionData> getActiveAuctionsByUserId(Long id) {
        try {
            return AuctionDataMapper.mapAuctionDatalistFromEnytity(auctionDao.getActiveAuctionsByUserId(id));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
    
     public List<AuctionData> getArchiveAuctionsByUserId(Long id) {
        try {
            return AuctionDataMapper.mapAuctionDatalistFromEnytity(auctionDao.getArchiveAuctionsByUserId(id));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new ArrayList<>();
        }
    }
     
     
     
     
    
    
    public AuctionData getAuctionDataById(Long id) {
        try {
            return AuctionDataMapper.mapAuctionDataFromEntity(auctionDao.getAuctionById(id));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new AuctionData();
        }
    }
    
    public AuctionData createAuctionDataFromRequest(HttpServletRequest request) {
        UserData currentUser = findUserForAuction(request);
        
        return AuctionData.builder()
                .carId(Long.valueOf(request.getParameter(EvConstants.CAR_ID)))
                .carTitle(request.getParameter(EvConstants.CAR_TITLE))
                .city(request.getParameter(EvConstants.CITY))
                .condition(request.getParameter(EvConstants.CAR_CONDITION))
                .creationDate(Date.valueOf(LocalDate.now()))
                .description(request.getParameter(EvConstants.DESCRIPTION))
                .mileage(Integer.valueOf(request.getParameter(EvConstants.MILEAGE)))
                .price(Integer.valueOf(request.getParameter(EvConstants.PRICE)))
                .prodYear(Integer.valueOf(request.getParameter(EvConstants.PRODUCTION_YEAR)))
                .active(true)
                .user(currentUser)
                .build();
    }
    
    public UserData findUserForAuction(HttpServletRequest request) {
        try {
            return UserDataMapper.mapUserDataFromEntity(userDao.getUserByName(request.getRemoteUser()));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return new UserData();
        }
    }
    
    public void archiveAuctionById(Long id){
        auctionDao.archiveAuctionById(id);
    }
}
