/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.admin;

import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.google.common.annotations.VisibleForTesting;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author danielbodi
 */
@Stateless
public class ManageUserService {
    
    @Inject
    private UserDao userDao;

    @VisibleForTesting
    void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    
    public void removeUser(String username) {
        User user = userDao.getUserByName(username);
        userDao.removeUser(user);
    }
    
    public void promoteUserToAdmin(String username) {
        User user = userDao.getUserByName(username);
        user.getGroup().setGroupname(EvConstants.ADMIN);
        userDao.updateUser(user);
    }
    
    public void demoteAdminToUser(String username) {
        User user = userDao.getUserByName(username);
        user.getGroup().setGroupname(EvConstants.USER);
        userDao.updateUser(user);
    }
    
    public List<String> getAllUsername() {
        return userDao.getAllUsername();
    }
    
    public List<String> getAllAdminName() {
        return userDao.getAllAdminName();
    }
}
