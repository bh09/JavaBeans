/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.entity.auction.Auction;
import com.braininghub.electricvehicleproject.entity.auction.AuctionImage;
import com.braininghub.electricvehicleproject.service.user.UserDataMapper;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel
 */
public class AuctionDataMapper {
    
    public static AuctionData mapAuctionDataFromEntity(Auction auction)
            throws IllegalAccessException, InvocationTargetException {
        
        return AuctionData.builder()
                .carId(auction.getCarId())
                .carTitle(auction.getCarTitle())
                .city(auction.getCity())
                .condition(auction.getCondition())
                .creationDate(auction.getCreationDate())
                .description(auction.getDescription())
                .mileage(auction.getMileage())
                .price(auction.getPrice())
                .prodYear(auction.getProdYear())
                .user(UserDataMapper.mapUserDataFromEntity(auction.getUser()))
                .images(mapAuctionImageDataFromEntity(auction.getImages()))
                .id(auction.getId())
                .active(auction.isActive())
                .build();
    }
    
    public static List<String> mapAuctionImageDataFromEntity(List<AuctionImage> images) {
        List<String> filenames = new ArrayList<>();
        
        for (AuctionImage image : images) {
            filenames.add(image.getFilename());
        }
        return filenames;
    }
    
    public static List<AuctionData> mapAuctionDatalistFromEnytity(List<Auction> auctionList)
            throws IllegalAccessException, InvocationTargetException {
        
        List<AuctionData> auctionDataList = new ArrayList<>();
        
        for (Auction auction : auctionList) {
            auctionDataList.add(mapAuctionDataFromEntity(auction));
        }
        return auctionDataList;
    }
}
