package com.braininghub.electricvehicleproject.service.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Roland Schablik
 */
public class MD5 {

    public static String hashPassword(String password) throws NoSuchAlgorithmException {
                
        String generatedPassword;

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] bytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        generatedPassword = sb.toString();

        return generatedPassword;
    }
}
