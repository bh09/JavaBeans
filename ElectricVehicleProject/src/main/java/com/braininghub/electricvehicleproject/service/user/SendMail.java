package com.braininghub.electricvehicleproject.service.user;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Roland Schablik
 */
public class SendMail {

    public static void send(String to, String sub, String msg) {

        final String username = "evcars.jb@gmail.com";
        final String password = "Javabeans";

        Properties props = new Properties();

        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(sub);
            message.setText(msg);

            Transport.send(message);

        } catch (MessagingException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String message(String username) {
        return "Dear " + username + ",\nthanks for your registration on "
                + "JavaBeans' Electric Cars community page.";
    }

    public static boolean isValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }
    
    public static String messageAuction(String emailFrom, String usernameFrom, String usernameTo) {
        return "Dear " + usernameTo + ",\nOur member, -" + usernameFrom
                + "- is interested in your auction. Contact: " + emailFrom + "\nBest regards\nJavaBeans Team";
    }
}
