/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.admin;

import com.braininghub.electricvehicleproject.dao.car.CarDao;
import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.service.car.CarEntityMapper;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.google.common.annotations.VisibleForTesting;
import java.lang.reflect.InvocationTargetException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author danielbodi
 */
@Stateless
public class ManageCarService {
        
    @Inject
    private CarDao carDao;

    @VisibleForTesting  
    void setCarDao(CarDao carDao) {
        this.carDao = carDao;
    }
    
    public void removeCarById(Long id) {
        Car car = carDao.getCarById(id);
        carDao.removeCar(car);
    }
    
    public void insertCar(CarData carData) {
        try {
            carDao.insertCar(CarEntityMapper.mapCarFromData(carData));
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Error during car insert.");
        }
    }
    
    public void updateCar(CarData carData, Long id) {
        try {
            Car car = carDao.getCarById(id);
            CarEntityMapper.updateCarFromData(car, carData);
            carDao.updateCar(car);
            
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Error during car update.");
        }
    }
    
    public BatteryData createBatteryDataFromRequest(HttpServletRequest request) {
        return BatteryData.builder()
                .capacity(Double.valueOf(request.getParameter(EvConstants.CAPACITY)))
                .chargePortType(request.getParameter(EvConstants.CHARGE_PORT_TYPE))
                .chargeSpeed(Integer.valueOf(request.getParameter(EvConstants.CHARGE_SPEED)))
                .fastchargePortType(request.getParameter(EvConstants.FASTCHARGE_PORT_TYPE))
                .fastchargeSpeed(Integer.valueOf(request.getParameter(EvConstants.FASTCHARGE_SPEED)))
                .build();
    }
    
    public ConsumptionData createConsumptionDataFromRequest(HttpServletRequest request) {
        return ConsumptionData.builder()
                .consumptionEPA(Double.valueOf(request.getParameter(EvConstants.CONSUMPTION_EPA)))
                .consumptionNEDC(Double.valueOf(request.getParameter(EvConstants.CONSUMPTION_NEDC)))
                .consumptionWLTP(Double.valueOf(request.getParameter(EvConstants.CONSUMPTION_WLTP)))
                .fuelEquivalentEPA(Double.valueOf(request.getParameter(EvConstants.FUEL_EQUIVALENT_EPA)))
                .fuelEquivalentNEDC(Double.valueOf(request.getParameter(EvConstants.FUEL_EQUIVALENT_NEDC)))
                .fuelEquivalentWLTP(Double.valueOf(request.getParameter(EvConstants.FUEL_EQUIVALENT_WLTP)))
                .rangeEPA(Integer.valueOf(request.getParameter(EvConstants.RANGE_EPA)))
                .rangeNEDC(Integer.valueOf(request.getParameter(EvConstants.RANGE_NEDC)))
                .rangeWLTP(Integer.valueOf(request.getParameter(EvConstants.RANGE_WLTP)))
                .build();
    }
    
    public DimensionsData createDimensionsDataFromRequest(HttpServletRequest request) {
        return DimensionsData.builder()
                .height(Integer.valueOf(request.getParameter(EvConstants.HEIGHT)))
                .length(Integer.valueOf(request.getParameter(EvConstants.LENGTH)))
                .weight(Integer.valueOf(request.getParameter(EvConstants.WEIGHT)))
                .width(Integer.valueOf(request.getParameter(EvConstants.WIDTH)))
                .build();
    }
        
    public PerformanceData createPerformanceDataFromRequest(HttpServletRequest request) {
        return PerformanceData.builder()
                .acceleration(Double.valueOf(request.getParameter(EvConstants.ACCELERATION)))
                .drivetrain(request.getParameter(EvConstants.DRIVETRAIN))
                .powerHp(Integer.valueOf(request.getParameter(EvConstants.POWER_HP)))
                .powerKw(Integer.valueOf(request.getParameter(EvConstants.POWER_KW)))
                .topSpeed(Integer.valueOf(request.getParameter(EvConstants.TOP_SPEED)))
                .torque(Integer.valueOf(request.getParameter(EvConstants.TORQUE)))
                .build();
    }
    
}
