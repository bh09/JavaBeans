package com.braininghub.electricvehicleproject.service.car;

import com.braininghub.electricvehicleproject.dto.car.EvParam;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.google.common.annotations.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Roland Schablik
 */
@Stateless
public class Calculator {

    @Inject
    private CarService carService;

    @VisibleForTesting
    void setCarService(CarService carService) {
        this.carService = carService;
    }

    public List<Integer> petrolCalculator(EvParam params) {

        List<Integer> petrolVehicle = new ArrayList<>();
        
        double petrolCost = Double.valueOf(params.getPetrolCost());
        double petrolCostIncreases = Double.valueOf(params.getPetrolCostIncreases());
        double distance = Double.valueOf(params.getDistance());
        double petrolConsumption = Double.valueOf(params.getPetrolConsumption());
        double cost = Double.valueOf(params.getPetrolVehicleCost());

        petrolVehicle.add(0, (int) cost);

        for (int i = 1; i < 11; i++) {
            petrolCost = petrolCost * (1 + petrolCostIncreases);
            cost += petrolConsumption * distance / 100 * petrolCost;
            petrolVehicle.add(i, (int) cost);
        }
        
        return petrolVehicle;

    }

    public List<Integer> electricCalculator(EvParam params) {

        List<Integer> electricVehicle = new ArrayList<>();

        CarData currentCar = carService.getCarDataById(Long.valueOf(params.getCarId()));

        double actualElectricityCost = Double.valueOf(params.getElectricityCost());
        double distance = Double.valueOf(params.getDistance());
        double consumption = currentCar.getConsumption().getConsumptionEPA();
        double cost = currentCar.getPrice();

        electricVehicle.add(0, (int) cost);

        for (int i = 1; i < 11; i++) {
            cost += consumption * distance / 100 * actualElectricityCost;
            electricVehicle.add(i, (int) cost);
        }
        
        return electricVehicle;
    }

    public String electricName(String carId) {
        
        CarData currentCar = carService.getCarDataById(Long.valueOf(carId));
        String carName = currentCar.getManufacturer() + " " + currentCar.getModel();

        return carName;

    }

}
