/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author daniel
 */
public class UserEntityMapper {
    
    public static User mapUserFromData(UserData userData)
            throws IllegalAccessException, InvocationTargetException {
        
        User user = new User();
        
        user.setGroupReference(mapGroupFromData(userData.getGroup()));
        user.setUsername(userData.getUsername());
        user.setPassword(userData.getPassword());
        user.setRegDate(userData.getRegDate());
        user.setEmail(userData.getEmail());
        user.setPhone(userData.getPhone());
        return user;
    }
    
    public static Group mapGroupFromData(GroupData groupData)
            throws IllegalAccessException, InvocationTargetException {
        
        Group group = new Group();
        BeanUtils.copyProperties(group, groupData);
        return group;
    }
    
    public static Group updateGroupFromData(Group group, GroupData groupData)
            throws IllegalAccessException, InvocationTargetException {
        
        BeanUtils.copyProperties(group, groupData);
        return group;
    }
    
}
