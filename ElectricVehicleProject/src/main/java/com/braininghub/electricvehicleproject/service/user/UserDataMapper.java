/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.user;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.user.Group;
import com.braininghub.electricvehicleproject.entity.user.User;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author daniel
 */
public class UserDataMapper {

    public static UserData mapUserDataFromEntity(User user)
            throws IllegalAccessException, InvocationTargetException {

        return UserData.builder()
                .group(mapGroupDataFromEntity(user.getGroup()))
                .username(user.getUsername())
                .password(user.getPassword())
                .regDate(user.getRegDate())
                .phone(user.getPhone())
                .email(user.getEmail())
                .id(user.getId())
                .build();
    }

    public static GroupData mapGroupDataFromEntity(Group group)
            throws IllegalAccessException, InvocationTargetException {

        GroupData groupData = new GroupData();
        BeanUtils.copyProperties(groupData, group);
        return groupData;
    }
    
    public static List<UserData> mapUserDataListFromEntity(List<User> userList)
            throws IllegalAccessException, InvocationTargetException {

        List<UserData> userDataList = new ArrayList<>();

        for (User user : userList) {
            userDataList.add(UserDataMapper.mapUserDataFromEntity(user));
        }
        return userDataList;
    }

}
