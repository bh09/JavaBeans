/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.service.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.entity.auction.Auction;
import com.braininghub.electricvehicleproject.entity.auction.AuctionImage;
import com.braininghub.electricvehicleproject.service.user.UserEntityMapper;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel
 */
public class AuctionEntityMapper {
    
    public static Auction mapAuctionFromData(AuctionData auctionData)
            throws IllegalAccessException, InvocationTargetException {
        
        Auction auction = Auction.builder()
                .carId(auctionData.getCarId())
                .carTitle(auctionData.getCarTitle())
                .city(auctionData.getCity())
                .condition(auctionData.getCondition())
                .creationDate(auctionData.getCreationDate())
                .description(auctionData.getDescription())
                .mileage(auctionData.getMileage())
                .price(auctionData.getPrice())
                .prodYear(auctionData.getProdYear())
                .user(UserEntityMapper.mapUserFromData(auctionData.getUser()))
                .active(auctionData.isActive())
                .id(auctionData.getId())
                .build();
        
        auction.setImagesReference(mapAuctionImageListFromData(auctionData.getImages()));
        return auction;
    }
    
    public static List<AuctionImage> mapAuctionImageListFromData(List<String> filenames) {
        List<AuctionImage> images = new ArrayList<>();
        
        for (String filename : filenames) {
            AuctionImage auctionImage = new AuctionImage();
            auctionImage.setFilename(filename);
            images.add(auctionImage);
        }
        return images;
    }
    
   
    
}
