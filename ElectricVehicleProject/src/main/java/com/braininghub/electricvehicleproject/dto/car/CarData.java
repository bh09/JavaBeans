/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.car;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CarData {
    
    private Long id;    
    private String manufacturer;
    private String model;
    private int price;
    
    private BatteryData battery;
    private ConsumptionData consumption;
    private PerformanceData performance;
    private DimensionsData dimensions;
    private List<ImageData> images;
    
}
