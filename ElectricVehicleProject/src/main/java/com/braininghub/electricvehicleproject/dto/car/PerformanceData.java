/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PerformanceData {
    
    private double acceleration;
    private int topSpeed;
    private int powerHp;
    private int powerKw;
    private int torque;
    private String drivetrain;
    
}
