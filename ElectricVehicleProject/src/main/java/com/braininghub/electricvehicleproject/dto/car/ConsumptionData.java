/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsumptionData {
    
    private int rangeEPA;
    private double consumptionEPA;
    private double fuelEquivalentEPA;
    
    private int rangeNEDC;
    private double consumptionNEDC;
    private double fuelEquivalentNEDC;
    
    private int rangeWLTP;
    private double consumptionWLTP;
    private double fuelEquivalentWLTP;
    
}
