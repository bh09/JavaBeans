/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.auction;

import com.braininghub.electricvehicleproject.dto.user.UserData;
import java.sql.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author daniel
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuctionData {
    
    private Long id;
    private UserData user;
    private int price;
    private int mileage;
    private int prodYear;
    private Date creationDate;
    private String description;
    private String condition;
    private String city;
    private String carTitle;
    private Long carId;
    private List<String> images;
    private boolean active;
    
}
