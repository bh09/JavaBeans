/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dto.user;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author daniel
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserData {
    
    private Long id;
    private String email;
    private String username;
    private String password;

    private GroupData group;
    private Date regDate;
    private String phone;
    
}
