package com.braininghub.electricvehicleproject.dto.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvParam {

    private String carId;
    private String petrolVehicleCost;
    private String petrolCost;
    private String petrolCostIncreases;
    private String distance;
    private String petrolConsumption;
    private String electricityCost;
}
