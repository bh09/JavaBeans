/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ivany
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "user"}))
public class LoginServlet extends HttpServlet {
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String userName = request.getRemoteUser();
        LOGGER.debug("LoginServlet (doGet) - login: {}", userName);

        if (request.isUserInRole("admin")) {
            response.sendRedirect("manage");
        } else if (request.isUserInRole("user")) {
            response.sendRedirect("carlist");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
