/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.service.auction.AuctionService;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.braininghub.electricvehicleproject.util.EvUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author daniel
 */
@MultipartConfig(location = "C:\\Workspace\\JavaBeans\\JavaBeans\\ElectricVehicleProject\\src\\main\\webapp\\asset\\auction_autos", 
        fileSizeThreshold=1024*1024, maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
@WebServlet(name = "CreateAuctionServlet", urlPatterns = {"/createauction"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "user"}))
public class CreateAuctionServlet extends HttpServlet {
    
    @Inject
    private AuctionService auctionService;
    
    @Inject
    private CarService carService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute(EvConstants.CAR_LIST, carService.getAllCarData());
        request.getRequestDispatcher("/WEB-INF/createauction.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setCharacterEncoding("UTF-8");
        
        AuctionData auctionData = auctionService.createAuctionDataFromRequest(request);
        List<String> images = new ArrayList<>();
        
        for (Part part : request.getParts()) {
            String fileName = EvUtil.extractFileName(part);
            if (!fileName.isEmpty()) {
                part.write(fileName);
                images.add(fileName);
            }
        }
        
        auctionData.setImages(images);
        auctionService.insertAuction(auctionData);
        
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }
    
}
