/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ivany
 */
@Data
@NoArgsConstructor
public class SelectBean {
    
    private boolean succesfulSelect;

    public void resetBean(){
        this.succesfulSelect = false;
    }
    
}
