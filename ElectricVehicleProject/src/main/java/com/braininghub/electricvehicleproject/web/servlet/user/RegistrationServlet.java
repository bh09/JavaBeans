/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.user;

import com.braininghub.electricvehicleproject.dto.user.GroupData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.service.user.MD5;
import com.braininghub.electricvehicleproject.service.user.SendMail;
import com.braininghub.electricvehicleproject.service.user.UserService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDate;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author danielbodi
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {

    @Inject
    private UserService userService;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private String subTitle;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.info("RegistrationServlet (doGet)");

        if (subTitle == null) {
            subTitle = "Fill out the form";
        }

        request.setAttribute("subTitle", subTitle);

        request.getRequestDispatcher("/WEB-INF/registration.jsp").forward(request, response);

        subTitle = null;

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String email = request.getParameter(EvConstants.EMAIL);
        String username = request.getParameter(EvConstants.USERNAME);
        String password = request.getParameter(EvConstants.PASSWORD);
        String password2 = request.getParameter(EvConstants.PASSWORD2);

        String eLeft = "<div class=\"fw400 cred\">";
        String eRight = "</div>";
        Date date = Date.valueOf(LocalDate.now());

        if (checkRegLen(username) && checkRegLen(password) && checkRegLen(password2)) {
            if (userService.emailCheck(email).isEmpty()
                    && userService.usernameCheck(username).isEmpty()) {
                if (SendMail.isValid(email)) {
                    if (password.equals(password2)) {
                        GroupData groupData = GroupData.builder()
                                .username(username)
                                .groupname(EvConstants.USER)
                                .build();
                        try {
                            String generatedPW = MD5.hashPassword(password);

                            UserData userData = UserData.builder()
                                    .username(username)
                                    .password(generatedPW)
                                    .email(email)
                                    .group(groupData)
                                    .regDate(date)
                                    .build();

                            userService.insertUser(userData);
                        } catch (NoSuchAlgorithmException ex) {
                            System.out.println(ex.getMessage());
                        }

                        LOGGER.info("Successful registration with {} as {}", email, username);
                        SendMail.send(email,
                                EvConstants.REGISTRATION_SUBJECT,
                                SendMail.message(username));
                        LOGGER.info("RegistrationServlet (doPost): {}", "Successful registration.");
                        request.getRequestDispatcher("/WEB-INF/successfulreg.jsp").forward(request, response);
                        return;
                    } else {
                        subTitle = eLeft + "Password does not match" + eRight;
                    }
                } else {
                    subTitle = eLeft + "Wrong email format, please try again" + eRight;
                }
            } else {
                subTitle = eLeft + "Username or email address already exist" + eRight;
            }
        } else {
            subTitle = eLeft + "Username or password is to short (min 6)" + eRight;
        }

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    private boolean checkRegLen(String str) {
        return str.trim().length() >= 6;
    }

}
