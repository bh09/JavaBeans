/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.service.auction.AuctionService;
import com.braininghub.electricvehicleproject.service.user.SendMail;
import com.braininghub.electricvehicleproject.service.user.UserService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author
 */
@WebServlet(name = "SendAuctionMail", urlPatterns = {"/sendauctionmail"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class SendAuctionMailServlet extends HttpServlet {

    @Inject
    private AuctionService auctionService;

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long auctId = Long.valueOf(request.getParameter(EvConstants.AUCTION_ID));
        AuctionData auctionData = auctionService.getAuctionDataById(auctId);

        String auctionOwnerUserName = auctionData.getUser().getUsername();
        String auctionOwnerEmail = auctionData.getUser().getEmail();

        String loginUserName = request.getRemoteUser();
        UserData userData = userService.getUserByName(loginUserName);
        String loginEmail = userData.getEmail();

        SendMail.send(auctionOwnerEmail,
                EvConstants.AUCTION_SUBJECT,
                SendMail.messageAuction(loginEmail, loginUserName, auctionOwnerUserName));
        
        request.getRequestDispatcher("/WEB-INF/successfulsent.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
