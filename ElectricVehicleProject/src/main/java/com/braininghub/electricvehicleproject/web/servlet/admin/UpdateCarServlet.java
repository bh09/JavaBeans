/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.admin;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.service.admin.ManageCarService;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author danielbodi
 */
@WebServlet(name = "UpdateCarServlet", urlPatterns = {"/updatecar"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin"}))
public class UpdateCarServlet extends HttpServlet {
    
    @Inject
    private CarService carService;
    
    @Inject
    private ManageCarService manageCarService;
    
    private CarData carData;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        carData = carService.getCarDataById(Long.valueOf(request.getParameter(EvConstants.ID)));
        request.setAttribute(EvConstants.CAR, carData);
        
        request.getRequestDispatcher("/WEB-INF/admin/updatecar.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        BatteryData batteryData = manageCarService.createBatteryDataFromRequest(request);
        ConsumptionData consumptionData = manageCarService.createConsumptionDataFromRequest(request);
        DimensionsData dimensionsData = manageCarService.createDimensionsDataFromRequest(request);
        PerformanceData performanceData = manageCarService.createPerformanceDataFromRequest(request);
        
        carData.setBattery(batteryData);
        carData.setConsumption(consumptionData);
        carData.setDimensions(dimensionsData);
        carData.setPerformance(performanceData);
        carData.setManufacturer(request.getParameter(EvConstants.MANUFACTURER));
        carData.setModel(request.getParameter(EvConstants.MODEL));
        carData.setPrice(Integer.valueOf(request.getParameter(EvConstants.PRICE)));
        
        manageCarService.updateCar(carData, Long.valueOf(request.getParameter(EvConstants.ID)));

        request.getRequestDispatcher("manage").forward(request, response);
    }
    
}
