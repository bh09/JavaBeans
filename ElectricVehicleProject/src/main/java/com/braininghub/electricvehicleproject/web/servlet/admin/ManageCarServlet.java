/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.admin;

import com.braininghub.electricvehicleproject.dto.car.BatteryData;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.dto.car.ConsumptionData;
import com.braininghub.electricvehicleproject.dto.car.DimensionsData;
import com.braininghub.electricvehicleproject.dto.car.ImageData;
import com.braininghub.electricvehicleproject.dto.car.PerformanceData;
import com.braininghub.electricvehicleproject.service.admin.ManageCarService;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.braininghub.electricvehicleproject.util.EvUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author daniel
 */
@MultipartConfig(location = "C:\\Workspace\\JavaBeans\\JavaBeans\\ElectricVehicleProject\\src\\main\\webapp\\asset\\autos", 
        fileSizeThreshold=1024*1024, maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
@WebServlet(name = "ManageCarServlet", urlPatterns = {"/manage"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin"}))
public class ManageCarServlet extends HttpServlet {

    @Inject
    private ManageCarService manageCarService;

    @Inject
    private CarService carService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute(EvConstants.CAR_LIST, carService.getAllCarData());
        request.getRequestDispatcher("/WEB-INF/admin/managecar.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if ("delete".equals(request.getParameter("action"))) {
            doDelete(request, response);
            return;
        }

        if ("create".equals(request.getParameter("action"))) {
            doPut(request, response);
            return;
        }

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BatteryData batteryData = manageCarService.createBatteryDataFromRequest(request);
        ConsumptionData consumptionData = manageCarService.createConsumptionDataFromRequest(request);
        DimensionsData dimensionsData = manageCarService.createDimensionsDataFromRequest(request);
        PerformanceData performanceData = manageCarService.createPerformanceDataFromRequest(request);

        List<ImageData> images = new ArrayList<>();
        
        for (Part part : request.getParts()) {
            String fileName = EvUtil.extractFileName(part);
            if (!fileName.isEmpty()) {
                part.write(fileName);
                ImageData imageData = new ImageData();
                imageData.setFilename(fileName);
                images.add(imageData);
            }
            
        }

        CarData carData = CarData.builder()
                .manufacturer(request.getParameter(EvConstants.MANUFACTURER))
                .model(request.getParameter(EvConstants.MODEL))
                .price(Integer.valueOf(request.getParameter(EvConstants.PRICE)))
                .battery(batteryData)
                .consumption(consumptionData)
                .dimensions(dimensionsData)
                .performance(performanceData)
                .images(images)
                .build();
        
        manageCarService.insertCar(carData);

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        manageCarService.removeCarById(Long.valueOf(request.getParameter(EvConstants.ID)));
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
