/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.user;

import com.braininghub.electricvehicleproject.dao.car.UserDao;
import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.dto.user.UserData;
import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.service.auction.AuctionService;
import com.braininghub.electricvehicleproject.service.user.UserEntityMapper;
import com.braininghub.electricvehicleproject.service.user.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ivany
 */
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "user"}))
@WebServlet(name = "profile", urlPatterns = {"/profile"})
public class ProfileServlet extends HttpServlet {

    @Inject
    private AuctionService auctionService;

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userName = request.getRemoteUser();

        UserData user = userService.getUserByName(userName);
        List<AuctionData> activeAuctions = auctionService.getActiveAuctionsByUserId(user.getId());
        List<AuctionData> archiveAuctions = auctionService.getArchiveAuctionsByUserId(user.getId());

        request.setAttribute("activeList", activeAuctions);
        request.setAttribute("archiveList", archiveAuctions);
        request.setAttribute("currentUser", user);

        System.out.println(user);
        System.out.println(activeAuctions);
        System.out.println(archiveAuctions);

        request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        auctionService.archiveAuctionById(Long.valueOf(request.getParameter("activeauctionid")));

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
