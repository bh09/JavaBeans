/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.car;

import com.braininghub.electricvehicleproject.web.bean.SelectBean;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "CarComparsionServlet", urlPatterns = {"/carcomparsion"})
public class CarComparsionServlet extends HttpServlet {

    @Inject
    private CarService service;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private List<CarData> carList;
    private CarData carData1;
    private CarData carData2;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.info("CarComparsionServlet (doGet)");

        SelectBean selectBean = new SelectBean();

        carList = service.getAllCarData();
        request.setAttribute(EvConstants.CAR_LIST, carList);

        setRequestAttributes(request, selectBean);

        request.getRequestDispatcher("/WEB-INF/carcomparsionmenu.jsp").forward(request, response);
        resetCarData();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        carData1 = service.getCarDataById(Long.valueOf(request.getParameter(EvConstants.CAR_ONE)));
        carData2 = service.getCarDataById(Long.valueOf(request.getParameter(EvConstants.CAR_TWO)));

        String model1 = carData1.getManufacturer() + " " + carData1.getModel();
        String model2 = carData2.getManufacturer() + " " + carData2.getModel();
        LOGGER.info("CarComparsionServlet (doPost): {} and {}", model1, model2);

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    private void setRequestAttributes(HttpServletRequest request, SelectBean bean) {
        if (carData1 != null && carData2 != null) {
            bean.setSuccesfulSelect(true);
            request.setAttribute(EvConstants.SELECT_BEAN, bean);
            request.setAttribute(EvConstants.CAR_ONE, carData1);
            request.setAttribute(EvConstants.CAR_TWO, carData2);
        }
    }

    private void resetCarData() {
        carData1 = null;
        carData2 = null;
    }

}
