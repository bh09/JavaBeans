/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.car;

import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "SearchServlet", urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {

    @Inject
    private CarService service;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private List<CarData> carList;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.info("SearchServlet (doGet)");

        request.setAttribute(EvConstants.CAR_LIST, carList);

        request.getRequestDispatcher("/WEB-INF/searchcar.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String search = request.getParameter("filter");

        carList = service.getCarDataBySearchParam(search);

        LOGGER.info("SearchServlet (doPost): {}", search);

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
