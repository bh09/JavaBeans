package com.braininghub.electricvehicleproject.web.servlet.car;

import com.braininghub.electricvehicleproject.dto.car.EvParam;
import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.service.car.Calculator;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import com.braininghub.electricvehicleproject.web.bean.SelectBean;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "ElectricVSPetrolServlet", urlPatterns = {"/electricvspetrol"})
public class ElectricVSPetrolServlet extends HttpServlet {

    @Inject
    private CarService service;

    @Inject
    private Calculator calculator;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private List<CarData> carList;
    private List<Integer> petrolCarData;
    private List<Integer> electricCarData;
    private String carName;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.info("ElectricVSPetrolServlet (doGet)");
        SelectBean selectBean = new SelectBean();

        carList = service.getAllCarData();
        request.setAttribute(EvConstants.CAR_LIST, carList);
        setRequestAttributes(request, selectBean);

        request.getRequestDispatcher("/WEB-INF/electricvspetrol.jsp").forward(request, response);
        resetCarData();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        EvParam params = setUpEvPparam(request);

        petrolCarData = calculator.petrolCalculator(params);
        electricCarData = calculator.electricCalculator(params);
        carName = calculator.electricName(params.getCarId());

        LOGGER.info("ElectricVSPetrolServlet (doPost): {}", carName);
        
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));

    }

    private EvParam setUpEvPparam(HttpServletRequest request) {

        return EvParam.builder()
                .carId(request.getParameter("car"))
                .petrolVehicleCost(request.getParameter("petrol_vehicle_cost"))
                .petrolCost(request.getParameter("petrol_cost"))
                .petrolCostIncreases(request.getParameter("petrol_cost_increases"))
                .distance(request.getParameter("distance"))
                .petrolConsumption(request.getParameter("petrol_consumption"))
                .electricityCost(request.getParameter("electricity_cost"))
                .build();

    }

    private void setRequestAttributes(HttpServletRequest request, SelectBean bean) {
        if (petrolCarData != null && electricCarData != null && carName != null) {
            bean.setSuccesfulSelect(true);
            request.setAttribute(EvConstants.SELECT_BEAN, bean);
            request.setAttribute("petrolCarData", petrolCarData);
            request.setAttribute("electricCarData", electricCarData);
            request.setAttribute("carName", carName);
        }
    }

    private void resetCarData() {
        petrolCarData = null;
        electricCarData = null;
        carName = null;
    }

}
