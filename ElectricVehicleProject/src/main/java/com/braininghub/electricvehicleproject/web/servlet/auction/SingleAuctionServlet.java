/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.auction;

import com.braininghub.electricvehicleproject.dto.auction.AuctionData;
import com.braininghub.electricvehicleproject.service.auction.AuctionService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author daniel
 */
@WebServlet(name = "SingleAuctionServlet", urlPatterns = {"/auction"})
public class SingleAuctionServlet extends HttpServlet {
    
    @Inject
    private AuctionService auctionService;
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        LOGGER.info("GET: doGet()");
        
        AuctionData auctionData = auctionService.getAuctionDataById(
                Long.valueOf(request.getParameter(EvConstants.ID)));
        
        request.setAttribute(EvConstants.AUCTION, auctionData);
        request.getRequestDispatcher("/WEB-INF/singleauction.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
