/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.admin;

import com.braininghub.electricvehicleproject.service.admin.ManageUserService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author danielbodi
 */
@WebServlet(name = "ManageUserServlet", urlPatterns = {"/manageuser"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin"}))
public class ManageUserServlet extends HttpServlet {
    
    @Inject
    private ManageUserService manageUserService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("usernameList", manageUserService.getAllUsername());
        request.setAttribute("adminNameList", manageUserService.getAllAdminName());
        request.getRequestDispatcher("/WEB-INF/admin/manageuser.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if ("delete".equals(request.getParameter("action"))) {
            doDelete(request, response);
            return;
        }
        
        if ("promote".equals(request.getParameter("action"))) {
            doPut(request, response);
            return;
        }
        
        manageUserService.demoteAdminToUser(request.getParameter("adminname"));
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        manageUserService.removeUser(request.getParameter(EvConstants.USERNAME));
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        manageUserService.promoteUserToAdmin(request.getParameter(EvConstants.USERNAME));
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }
    
}
