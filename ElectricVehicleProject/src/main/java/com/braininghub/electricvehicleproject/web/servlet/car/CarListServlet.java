package com.braininghub.electricvehicleproject.web.servlet.car;

import com.braininghub.electricvehicleproject.web.bean.SelectBean;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "CarListServlet", urlPatterns = {"/carlist"})
public class CarListServlet extends HttpServlet {

    @Inject
    private CarService service;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        LOGGER.info("CarListServlet (doGet)");

        SelectBean selectBean = new SelectBean();
        request.setAttribute(EvConstants.SELECT_BEAN, selectBean);
        
        request.setAttribute(EvConstants.MANUFACTURER_LIST, service.getManufacturerListForDropdown());
        request.setAttribute(EvConstants.CAR_LIST, service.getAllCarData());

        request.getRequestDispatcher("/WEB-INF/carlist.jsp").forward(request, response);
        selectBean.resetBean();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
