/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.web.servlet.car;

import com.braininghub.electricvehicleproject.dto.car.CarData;
import com.braininghub.electricvehicleproject.service.car.CarService;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "SingleCarServlet", urlPatterns = {"/singlecar"})
public class SingleCarServlet extends HttpServlet {

    @Inject
    private CarService service;
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Long carId = Long.valueOf(request.getParameter(EvConstants.ID));
        CarData carData = service.getCarDataById(carId);
        request.setAttribute(EvConstants.CAR, carData);
        
        LOGGER.info("SingleCarServlet (doGet): {}", carId);

        request.getRequestDispatcher("/WEB-INF/singlecar.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
