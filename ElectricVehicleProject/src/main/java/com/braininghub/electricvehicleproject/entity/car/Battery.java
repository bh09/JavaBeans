/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.car;

import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.BATTERY)
public class Battery implements Serializable {
    
    @Id
    private Long id;
    
    @OneToOne
    @JoinColumn
    @MapsId
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Car car;
    
    @Column(name = EvConstants.CAPACITY)
    private double capacity;
    
    @Column(name = EvConstants.CHARGE_PORT_TYPE)
    private String chargePortType;
    
    @Column(name = EvConstants.CHARGE_SPEED)
    private int chargeSpeed;
    
    @Column(name = EvConstants.FASTCHARGE_PORT_TYPE)
    private String fastchargePortType;
    
    @Column(name = EvConstants.FASTCHARGE_SPEED)
    private int fastchargeSpeed;
    
}
