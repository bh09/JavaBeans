/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.user;

import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author daniel
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.USER_TYPES)
public class Group implements Serializable {
    
    @Id
    private Long id;
    
    @MapsId
    @OneToOne
    @JoinColumn
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;

    @Column(name = EvConstants.USERNAME)
    private String username;
    
    @Column(name = EvConstants.GROUPNAME)
    private String groupname;
    
}
