/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.car;

import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.CONSUMPTION)
public class Consumption implements Serializable {
    
    @Id
    private Long id;
    
    @OneToOne
    @JoinColumn
    @MapsId
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Car car;
    
    @Column(name = EvConstants.RANGE_EPA)
    private int rangeEPA;
    
    @Column(name = EvConstants.CONSUMPTION_EPA)
    private double consumptionEPA;
    
    @Column(name = EvConstants.FUEL_EQUIVALENT_EPA)
    private double fuelEquivalentEPA;
    
    @Column(name = EvConstants.RANGE_NEDC)
    private int rangeNEDC;
    
    @Column(name = EvConstants.CONSUMPTION_NEDC)
    private double consumptionNEDC;
    
    @Column(name = EvConstants.FUEL_EQUIVALENT_NEDC)
    private double fuelEquivalentNEDC;
    
    @Column(name = EvConstants.RANGE_WLTP)
    private int rangeWLTP;
    
    @Column(name = EvConstants.CONSUMPTION_WLTP)
    private double consumptionWLTP;
    
    @Column(name = EvConstants.FUEL_EQUIVALENT_WLTP)
    private double fuelEquivalentWLTP;
    
}
