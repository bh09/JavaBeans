/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.car;

import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author danielbodi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.CAR)
@NamedQuery(name = "Car.findCarById", query = "SELECT a FROM Car a WHERE a.id = :id")
public class Car implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = EvConstants.MANUFACTURER)
    private String manufacturer;
    
    @Column(name = EvConstants.MODEL)
    private String model;
    
    @Column(name = EvConstants.PRICE)
    private int price;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = EvConstants.CAR)
    private Battery battery;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = EvConstants.CAR)
    private Consumption consumption;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = EvConstants.CAR)
    private Performance performance;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = EvConstants.CAR)
    private Dimensions dimensions;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = EvConstants.CAR)
    private List<Image> images;

    public void setBatteryReference(Battery battery) {
        battery.setCar(this);
        this.battery = battery;
    }

    public void setConsumptionReference(Consumption consumption) {
        consumption.setCar(this);
        this.consumption = consumption;
    }

    public void setPerformanceReference(Performance performance) {
        performance.setCar(this);
        this.performance = performance;
    }

    public void setDimensionsReference(Dimensions dimensions) {
        dimensions.setCar(this);
        this.dimensions = dimensions;
    }

    public void setImagesReference(List<Image> images) {
        images.forEach(p -> p.setCar(this));
        this.images = images;
    }
    
}
