/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.auction;

import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author daniel
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.AUCTION)
public class Auction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    private User user;
    
    @Column(name = EvConstants.PRICE)
    private int price;
    
    @Column(name = EvConstants.MILEAGE)
    private int mileage;
    
    @Column(name = EvConstants.PRODUCTION_YEAR)
    private int prodYear;
    
    @Column(name = EvConstants.DATE_ADDED)
    private Date creationDate;
    
    @Lob
    @Column(name = EvConstants.DESCRIPTION, length = 10000)
    private String description;
    
    @Column(name = EvConstants.CAR_CONDITION)
    private String condition;
    
    @Column(name = EvConstants.CITY)
    private String city;
    
    @Column(name = EvConstants.CAR_TITLE)
    private String carTitle;
    
    @Column(name = EvConstants.CAR_ID)
    private Long carId;
    
    @Column(name = "active")
    private boolean active;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = EvConstants.AUCTION)
    private List<AuctionImage> images;
    
    public void setImagesReference(List<AuctionImage> images) {
        images.forEach(p -> p.setAuction(this));
        this.images = images;
    }
    
}
