/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.entity.user;

import com.braininghub.electricvehicleproject.util.EvConstants;
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author daniel
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = EvConstants.USERS)
public class User implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = EvConstants.USER)
    private Group group;

    @Column(name = EvConstants.USERNAME, nullable = false, unique = true)
    private String username;
    
    @Column(name = EvConstants.EMAIL, nullable = false, unique = true)
    private String email;
    
    @Column(name = EvConstants.PASSWORD, nullable = false)
    private String password;
    
    @Column(name = "regdate")
    private Date regDate;
    
    @Column(name ="phone")
    private String phone;

    public void setGroupReference(Group group) {
        group.setUser(this);
        this.group = group;
    }
    
}
