/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dao.car;

import com.braininghub.electricvehicleproject.entity.car.Car;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class CarDao {

    @PersistenceContext(unitName = EvConstants.PU_NAME)
    private EntityManager em;

    @Transactional
    public void insertCar(Car car) {
        em.persist(car);
    }
    
    @Transactional
    public void removeCar(Car car) {
        em.remove(car);
    }
    
    @Transactional
    public void updateCar(Car car) {
        em.merge(car);
    }

    public List<Car> getAllCars() {
        return em.createQuery("SELECT a FROM Car a ORDER BY a.manufacturer, a.model", Car.class)
                .getResultList();
    }

    public List<String> getManufacturerListForDropDown() {
        return em.createQuery("SELECT DISTINCT a.manufacturer FROM Car a ORDER BY a.manufacturer", String.class)
                .getResultList();
    }

    public List<String> getModelListForDropDown() {
        return em.createQuery("SELECT DISTINCT a.model FROM Car a ORDER BY a.model", String.class)
                .getResultList();
    }

    public Car getCarById(Long id) {
        return em.createNamedQuery("Car.findCarById", Car.class)
                .setParameter(EvConstants.ID, id)
                .getSingleResult();
    }

    public List<Car> getFilteredCars(String manufacturer, String model) {
        return em.createQuery("SELECT a FROM Car a WHERE a.manufacturer LIKE :manufacturer OR a.model LIKE :model", Car.class)
                .setParameter(EvConstants.MANUFACTURER, createWildcardParam(manufacturer))
                .setParameter(EvConstants.MODEL, createWildcardParam(model))
                .getResultList();
    }

    public List<Car> getCarsBySearchParam(String search) {
        return em.createQuery("SELECT a FROM Car a WHERE a.manufacturer LIKE :search OR a.model LIKE :search", Car.class)
                .setParameter("search", createWildcardParam(search))
                .getResultList();
    }

    private String createWildcardParam(String string) {
        return EvConstants.WILDCARD + string + EvConstants.WILDCARD;
    }

}
