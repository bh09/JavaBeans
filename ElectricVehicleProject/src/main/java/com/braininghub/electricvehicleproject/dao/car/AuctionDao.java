/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dao.car;

import com.braininghub.electricvehicleproject.entity.auction.Auction;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author daniel
 */
@Stateless
public class AuctionDao {
    
    @PersistenceContext(unitName = EvConstants.PU_NAME)
    private EntityManager em;
    
    @Transactional
    public void insertAuction(Auction auction) {
        em.persist(auction);
    }
    
    @Transactional
    public void deleteAuction(Auction auction) {
        em.remove(auction);
    }
    
    public List<Auction> getAllAuctions() {
        return em.createQuery("SELECT a FROM Auction a ORDER BY a.creationDate", Auction.class)
                .getResultList();
    }
    
    public List<Auction> getActiveAuctions() {
        return em.createQuery("SELECT a FROM Auction a WHERE a.active = true ORDER BY a.creationDate ", Auction.class)
                .getResultList();
    }
    
    public List<Auction> getArchiveAuctions() {
        return em.createQuery("SELECT a FROM Auction a WHERE a.active = false ORDER BY a.creationDate", Auction.class)
                .getResultList();
    }
    
    public Auction getAuctionById(Long id) {
        return em.createQuery("SELECT a FROM Auction a WHERE a.id = :id", Auction.class)
                .setParameter(EvConstants.ID, id)
                .getSingleResult();
    }
    
    public List<Auction> getActiveAuctionsByUserId(Long id) {
        return em.createQuery("SELECT a FROM Auction a WHERE a.active = true AND a.user.id = :id ORDER BY a.creationDate ", Auction.class)
                .setParameter(EvConstants.ID, id)
                .getResultList();
    }
    
    public void archiveAuctionById(Long id){
        em.createQuery("UPDATE Auction a SET a.active = false WHERE a.id = :id")
                .setParameter(EvConstants.ID, id)
                .executeUpdate();
    }
    
    public List<Auction> getArchiveAuctionsByUserId(Long id) {
        return em.createQuery("SELECT a FROM Auction a WHERE a.active = false AND a.user.id = :id ORDER BY a.creationDate", Auction.class)
                .setParameter(EvConstants.ID, id)
                .getResultList();
    }
    
}
