/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.braininghub.electricvehicleproject.dao.car;

import com.braininghub.electricvehicleproject.entity.user.User;
import com.braininghub.electricvehicleproject.util.EvConstants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author danielbodi
 */
@Stateless
public class UserDao {

    @PersistenceContext(unitName = EvConstants.PU_NAME)
    private EntityManager em;

    @Transactional
    public void insertUser(User user) {
        em.persist(user);
    }

    @Transactional
    public void removeUser(User user) {
        em.remove(user);
    }

    public List<User> isEmailExists(String email) {
        return em.createQuery("SELECT a FROM User a WHERE a.email = :email", User.class)
                .setParameter("email", email).getResultList();
    }

    public List<User> isUserExists(String username) {
        return em.createQuery("SELECT a FROM User a WHERE a.username = :username", User.class)
                .setParameter("username", username).getResultList();
    }

    @Transactional
    public void updateUser(User user) {
        em.merge(user);
    }

    public List<String> getAllUsername() {
        return em.createQuery("SELECT a.username FROM User a WHERE a.group.groupname = 'user' ORDER BY a.username", String.class)
                .getResultList();
    }

    public List<String> getAllAdminName() {
        return em.createQuery("SELECT a.username FROM User a WHERE a.group.groupname = 'admin' ORDER BY a.username", String.class)
                .getResultList();
    }

    public User getUserByName(String username) {
        return em.createQuery("SELECT a FROM User a WHERE a.username = :username", User.class)
                .setParameter(EvConstants.USERNAME, username)
                .getSingleResult();
    }

}
